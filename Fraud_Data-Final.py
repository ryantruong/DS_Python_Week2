
# coding: utf-8

# ### Objective
# ##### 1. Determine customer country based on the numeric IP address.
# ##### 2. Build a model to predict whether an activity is fraudulent or not
# ##### 3. Explain how different assumptions about the cost of false positives vs false negatives would impact the model.
# ##### 4. Explain the models in the most simply way
# ##### 5. Evaluate and choose the best fit model

# ### Data Sources
# ##### 1. “Fraud_data”: information about the user first transaction. 
# 
# Columns:
# 
# •	user_id: unique id of the user
# 
# •	Signup_time: the time when the user created account (GMT time)
# 
# •	purchase_time: the value of the first transaction (USD)
# 
# •	device_id: unique id by device, may have the case that 2 transactions with the same device_id.
# 
# •	source: Marketing channels (SEO, Ads, Direct)
# 
# •	browser: the browser user use to purchase
# 
# •	sex: user gender (Male/ Female)
# 
# •	age: user age
# 
# •	ip_address: user numeric ip address
# 
# •	class: idetify whether an activity was fraudulent(1) or not (0)
# 
# 
# 
# ##### 2. “IpAddress_to_Country”: the list of ip address with its country. Each country have a range and a numeric ip address falls within the range will belong to that country
# 
# Columns:
# 
# •	lower_bound_ip_address: the lower bound numeric ip address
# 
# •	upper_bound_ip_address: the upper bound numeric ip address
# 
# •	country: corresponding country

# ## Index
# ### A. Import library, data and data exploration
# ### B. Data Manipulate
# ### C. Exploratory Data Analysis
# ### D. Data Preprocessing
# ### E. Feature Selection
# ### F. Predictive Models
# ### G. Model Evaluation
# ### H. Analysis

# ### A. Import library, data and data exploration

# In[1]:


get_ipython().system('pip install ipython_memwatcher')
get_ipython().system('pip install graphviz')
import pandas as pd
import numpy as np
from pandas import read_csv
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn import preprocessing
from sklearn.preprocessing import scale
from sklearn.preprocessing import StandardScaler

from sklearn.feature_selection import RFE
from sklearn.decomposition import PCA
from sklearn import decomposition
from sklearn.ensemble import ExtraTreesClassifier

import statsmodels.api as sm
from sklearn.linear_model import LogisticRegression

from sklearn.cross_validation import train_test_split
from sklearn import metrics
from sklearn import model_selection
from sklearn.model_selection import cross_val_score

from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree

import graphviz 
from sklearn.tree import export_graphviz

import os
os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin'


# In[2]:


#Import data from csv file
fraud_data= pd.read_csv('Fraud_Data.csv')
ip = pd.read_csv('IpAddress_to_Country.csv')


# In[3]:


pd.set_option('display.expand_frame_repr', False) #show all columns in dataset
pd.set_option('display.max_rows', 100)
pd.set_option('display.max_columns', 200)
pd.set_option('display.width', 1000)


# In[4]:


#Check 5 top and bottom rows
fraud_data.head()


# In[5]:


fraud_data.tail()


# In[6]:


#Check data infomation
fraud_data.info()


# In[7]:


#check number of Null value
fraud_data.isnull().sum()


# In[8]:


#Check 5 top and bottom rows
print (ip.head())
print(ip.tail())


# In[9]:


#Check data infomation
ip.info()


# In[10]:


#check number of Null value
ip.isnull().sum()


# ### B.  Data Manipulate

# #### 1. Find country

# In[11]:


data=fraud_data.sort_values("ip_address").reset_index(drop=True) #sort by ip_address for faster search
country=[]
min_ip=min(ip.iloc[:,0])
max_ip=max(ip.iloc[:,1])
#define a Funtion to find country code from an Ip Code
def get_ip(ip_code):
    if len(ip.loc[(ip.lower_bound_ip_address < ip_code) & (ip.upper_bound_ip_address>ip_code), 'country'])==0: #to filter the IP codes not in csv file
        return "None"
    else:
        return ip.loc[(ip.lower_bound_ip_address < ip_code) & (ip.upper_bound_ip_address>ip_code), 'country'].values[0] #return the country when Ip Code in the range from lower and upper level
# Because already sort the IP Code so gie the Ip Code lower or higher than the threshole a name of "None"
data['country'] = data.apply(lambda r: "None" if (r['ip_address'] < min_ip) or (r['ip_address'] > max_ip) else get_ip(r['ip_address']),axis=1)


# #### 2. Calculate interday from the day of sign up and purchase day

# In[12]:


data.signup_time=data.signup_time.astype('M8[us]') # Convert to timedate type
data.purchase_time=data.purchase_time.astype('M8[us]')  # Convert to timedate type
data['Interday_signup_purchase']=data.purchase_time-data.signup_time #Calculate Interday
data['Interday_signup_purchase']=data['Interday_signup_purchase'].dt.days #Rounded to number of days


# In[13]:


data.sample(100) # get sample of 100 cases


# ### C. Exploratory Data Analysis

# #### 1. Class

# In[14]:


get_ipython().run_line_magic('matplotlib', 'inline')
print(data['class'].value_counts())
sns.countplot(x='class',data=data)


# In[15]:


data.groupby('class').mean()


# ##### Comment:
# - The fraudulent cases is 9.4% total cases. This cause an imbalance in our data. In our model, we will consider using multiple models and performance metrics to cope with the imbalance data.
# - The average of purchase value and age of those 2 classes are quite similar but the average interday for fraudulent transactions are much smaller than valid ones. This suggests that interday will be one of the most important variables to predict fraud.

# #### 2. Class and Source

# In[16]:


get_ipython().run_line_magic('matplotlib', 'inline')
plt.gcf().set_size_inches(16,8)
table=pd.crosstab(data.source,data['class'])
table.plot(kind='bar',ax=plt.subplot(221))
plt.title('Fraud vs Source')
plt.xlabel('Source')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend
table.div(table.sum(1).astype(float), axis=0).plot(kind='bar',legend=None, stacked=True, ax=plt.subplot(222))
plt.title('Stacked Bar Chart of Fraud vs Source')
plt.xlabel('Source')
plt.ylabel('Proportion of Fraud')


# ##### Comment:
# - Transactions come mostly from Ads and SEO.
# - Proportions of fraud for 3 sources are quite similar but ratio for Direct transactions is a little bit higher

# #### 3. Fraud class and Browser

# In[17]:


get_ipython().run_line_magic('matplotlib', 'inline')
plt.gcf().set_size_inches(16,8)
table=pd.crosstab(data.browser,data['class'])
table.plot(kind='bar',ax=plt.subplot(221))
plt.title('Fraud vs Browser')
plt.xlabel('Browser')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend
table.div(table.sum(1).astype(float), axis=0).plot(kind='bar',legend=None, stacked=True, ax=plt.subplot(222))
plt.title('Stacked Bar Chart of Fraud vs Browser')
plt.xlabel('Browser')
plt.ylabel('Proportion of Fraud')


# ##### Comment:
# - Chrome is the most popular browser and Internet Explorer is in the second place. 
# - The proportions of fraud are in the same level between browsers but Chrome and Firefox are slightly bigger

# #### 4. Fraud and Gender

# In[18]:


get_ipython().run_line_magic('matplotlib', 'inline')
plt.gcf().set_size_inches(16,8)
table=pd.crosstab(data.sex,data['class'])
table.plot(kind='bar',ax=plt.subplot(221))
plt.title('Fraud vs Gender')
plt.xlabel('Gender')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend
table.div(table.sum(1).astype(float), axis=0).plot(kind='bar', legend=None, stacked=True, ax=plt.subplot(222))
plt.title('Stacked Bar Chart of Fraud vs Gender')
plt.xlabel('Gender')
plt.ylabel('Proportion of Fraud')


# ##### Comment:
# - More male customers than female
# - Transactions from male customers also at a bit higher risk than female

# #### 5. Fraud and Age

# In[19]:


print ("Average age: ", data.age.mean())
print ("Median age: ", data.age.median())


# In[20]:


get_ipython().run_line_magic('matplotlib', 'inline')
plt.gcf().set_size_inches(12,12)
table=pd.crosstab(data.age,data['class'])
table.plot(kind='bar',ax=plt.subplot(211))
plt.title('Fraud vs Age')
plt.xlabel('Age')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend

table.div(table.sum(1).astype(float), axis=0).plot(kind='bar',legend=None, stacked=True, ax=plt.subplot(212))
plt.title('Stacked Bar Chart of Fraud vs Age')
plt.xlabel('Age')
plt.ylabel('Proportion of Fraud')


# ##### Comment:
# - Customer age ranges from 18 to 76.
# - Average age is 33.14 and Median age:  33.0
# - Higher rate of fraud is from group of 63 y.o where around 30% transaction are fraudulent. Second risk group is 61 y.o with around 20% fraud

# #### 6. Fraud and Purchase value

# In[21]:


print ("Average purchase value: ", data.purchase_value.mean())
print ("Median purchase value: ", data.purchase_value.median())


# In[22]:


get_ipython().run_line_magic('matplotlib', 'inline')
plt.gcf().set_size_inches(18,12)
table=pd.crosstab(data.purchase_value,data['class'])
table.plot(kind='bar',ax=plt.subplot(211))
plt.title('Fraud vs Purchase_value')
plt.xlabel('Purchase_value')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend

table.div(table.sum(1).astype(float), axis=0).plot(kind='bar',legend=None, stacked=True, ax=plt.subplot(212))
plt.title('Stacked Bar Chart of Fraud vs Purchase_value')
plt.xlabel('Purchase_value')
plt.ylabel('Proportion of Fraud')


# ##### Comment:
# - Lowest purchase value is 9 and the highest is 154
# - Average purchase value is 36.94 and Median purchase value is 35.0
# - 3 most fraudulent transactions are with values of 100, 88 and 92 (25-30% of fraud)

# #### 7. Fraud and Country

# In[23]:


get_ipython().run_line_magic('matplotlib', 'inline')
plt.gcf().set_size_inches(16,10)
table=data.pivot_table(["user_id"],index='country',columns="class",aggfunc="count", margins=True)
table1=table.sort_values(by=('user_id','All'), ascending=False).iloc[1:11,0:2]
table1.plot(kind='barh', stacked=False, ax=plt.subplot(221))
plt.title('Top 10 Countries in number of customer')
plt.xlabel('Country')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend
table2=table.div(table.sum(1).astype(float), axis=0).sort_values(by=('user_id',1), ascending=False).iloc[1:11,0:2]
table2.plot(kind='barh', legend=None, stacked=True, ax=plt.subplot(222))
plt.title('Stacked Bar Chart of Fraud in top 10 countries')
plt.xlabel('Country')
plt.ylabel('Proportion of Fraud')


# ##### Comment:
# - Nearly 1/3 of the transactions are from United States. In second is the Non-identified IP address which is marked as "None" for country. So, the number of fraudulent transaction is also the highest.
# - In term of fraud rate, transactions from Namibia when nearly 50% of transactions are invalid. Second is Sri Lanka and surprisingly Luxembourg in third place. 
# 

# #### 8. Fraud and Days from sign up

# In[24]:


get_ipython().run_line_magic('matplotlib', 'inline')
plt.gcf().set_size_inches(18,12)
table=pd.crosstab(data.Interday_signup_purchase,data['class'])
table.plot(kind='bar',ax=plt.subplot(211))
plt.title('Fraud vs Interday from sign up')
plt.xlabel('Interday from sign up')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend

table.div(table.sum(1).astype(float), axis=0).plot(kind='bar',legend=None, stacked=True, ax=plt.subplot(212))
plt.title('Stacked Bar Chart of Fraud vs Interday from sign up')
plt.xlabel('Interday from sign up')
plt.ylabel('Proportion of Fraud')


# ##### Comment:
# - The distribution of interday is quite balance in ranges. But the number of fraudulent cases fro 0 interday is significantly high with nearly 8000 cases. As we mentioned above, interday is a very important predictor.
# - In very similar way, The rate of fraud for 0 interday is the highest at around 90%

# ### D. Data Preprocessing

# #### 1. Encoding categorical variables

# In[25]:


#source Ads = 0, Direct =1, SEO =2
le = preprocessing.LabelEncoder()
le.fit(data.source)
print (list(le.classes_))
data['source_code']=le.transform(data.source)


# In[26]:


#browser 'Chrome'=0, 'FireFox'=1, 'IE'=2, 'Opera'=3, 'Safari'=4
le = preprocessing.LabelEncoder()
le.fit(data.browser)
print (list(le.classes_))
data['browser_code']=le.transform(data.browser)


# In[27]:


#sex Female= 0, Male =1
le = preprocessing.LabelEncoder()
le.fit(data.sex)
print (list(le.classes_))
data['sex_code']=le.transform(data.sex)


# In[28]:


data.country.nunique()


# In[29]:


#There are 182 countries in our data, each country will receive a code
le = preprocessing.LabelEncoder()
le.fit(data.country)
print (list(le.classes_))
data['country_code']=le.transform(data.country)


# In[30]:


data.head()


# ### E. Feature Selection

# In[31]:


data.columns


# In[32]:


Y=data['class'] #Y: Target
X=data.drop(['user_id','class','signup_time', 'purchase_time', 'device_id', 'source', 'browser', 'sex', 'ip_address', 'country'], axis=1)
#X: Predictor


# ##### Comment
# - We will only use the variables with numeric values in our models so I only select those variables. 
# - In this step I seperate 

# #### 1. Pearson Correlation

# In[33]:


get_ipython().run_line_magic('matplotlib', 'inline')
# calculate the correlation matrix
corr = X.corr()
print(corr)
# plot the heatmap
sns.heatmap(corr, xticklabels=corr.columns, yticklabels=corr.columns)


# ##### Comment
# - There is no high correlated between features. So we can use all the variables.

# #### 2. Feature selection:

# In[34]:


model = LogisticRegression()
rfe = RFE(model, 7)
fit = rfe.fit(X,Y)
print(("Num Features: %d") % fit.n_features_)

print("Selected Features:")
print(pd.DataFrame(list(zip(X.columns.tolist(), fit.support_)),columns=["Var","Selection"]).set_index("Var").iloc[:,0:2])
print("Feature Ranking:")
print(pd.DataFrame(list(zip(X.columns.tolist(), fit.ranking_)),columns=["Var","Rank"]).set_index("Var").iloc[:,0:2])


# ##### Comment
# - We base on the Logistic regression to choose the feature by include each variable in the model and test the significance of the model. 
# - All 7 variables are selected in this case and all rank the same.

# #### 3. PCA

# In[35]:


get_ipython().run_line_magic('matplotlib', 'inline')
X_std = StandardScaler().fit_transform(X) #Standardize all values
pca = PCA(n_components=7)
fit = pca.fit(X_std)
# summarize components
print(("Explained Variance: %s") % fit.explained_variance_ratio_)
plt.plot(np.cumsum(fit.explained_variance_ratio_), c='red',marker = ".")
plt.xticks(np.arange(0, 10,2))
plt.gcf().set_size_inches(8,4)
plt.grid()
plt.xlabel('number of components')
plt.ylabel('cumulative explained variance')
plt.show()


# ##### Comment
# - In PCA, we need to standardize the data 
# - The variances for all 7 variables are similar so PCA can not help us to define new PCs. 

# #### 4. Importance Ranking

# In[36]:


get_ipython().run_line_magic('matplotlib', 'inline')
plt.gcf().set_size_inches(8,6)
model = ExtraTreesClassifier()
model.fit(X,Y)
importance= pd.DataFrame(list(zip(X.columns,model.feature_importances_)),columns=["Var","Importance"])
importance=importance.sort_values(by="Importance",ascending=False).set_index("Var")
print(importance)
importance.plot(kind='barh',legend= None)
plt.title('Variable Importance rank')
plt.xlabel('Importance')


# ##### Comment
# - Interday is the most important feature.
# - Second is Purchase value and third is age. 
# - Less important variable is sex_code.
# 
# 
# ##### => we include all features in the model

# ### F. Predictive Models

# #### 1. Logistic Regression

# In[37]:


logit_model=sm.Logit(Y,X)
result=logit_model.fit()
print(result.summary())


# In[38]:


X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=0)
logreg = LogisticRegression()
logreg.fit(X_train, y_train)


# #### 2. Decision Tree - Gini Index

# In[39]:


get_ipython().run_line_magic('matplotlib', 'inline')
gini = DecisionTreeClassifier(criterion = "gini", random_state = 100, max_depth=3, min_samples_leaf=5)
gini.fit(X_train, y_train)
dot_data = tree.export_graphviz(gini, out_file=None) 
graph = graphviz.Source(dot_data)
graph


# #### 3. Decision Tree- Entropy

# In[40]:


get_ipython().run_line_magic('matplotlib', 'inline')
entropy = DecisionTreeClassifier(criterion = "entropy", random_state = 100,max_depth=3, min_samples_leaf=5)
entropy.fit(X_train, y_train)
dot_data = tree.export_graphviz(entropy, out_file=None) 
graph = graphviz.Source(dot_data)
graph


# #### 4. Neural Network

# In[41]:


X_std_train, X_std_test, y_std_train, y_std_test = train_test_split(X_std, Y, test_size=0.3, random_state=0)
from sklearn.neural_network import MLPClassifier
NN = MLPClassifier(hidden_layer_sizes=(10,10,10))
NN.fit(X_std_train,y_std_train)


# #### 5. Random Forest

# In[42]:


from sklearn.ensemble import RandomForestClassifier
RF = RandomForestClassifier(n_estimators=100, oob_score=True, random_state=0)
RF.fit(X_train, y_train)
y_pred_RF = RF.predict(X_test)


# ### G. Model Evaluation

# #### 1. Logistic Regression

# In[43]:


print("1. Logistic Regression")
y_pred_log = logreg.predict(X_test)
y_prob_log = logreg.predict_proba(X_test)
print('a. Accuracy: {:.2f}'.format(logreg.score(X_test, y_test)*100))
kfold = model_selection.KFold(n_splits=10, random_state=7)
modelCV = LogisticRegression()
results = model_selection.cross_val_score(modelCV, X_train, y_train, cv=kfold, scoring='accuracy')
print("b. Accuracy for 10-fold cross validation: %.2f" % (results.mean()*100))
print("c. Confusion matrix:")
print(confusion_matrix(y_test, y_pred_log))
print("d. Classification Report")
print(classification_report(y_test, y_pred_log))


# #### 2. Decision Tree - Gini Index

# In[44]:


print("2. Decision Tree- Gini Index")
y_pred_gini = gini.predict(X_test)
y_prob_gini= gini.predict_proba(X_test)
print('a. Accuracy: {:.3f}'.format(accuracy_score(y_test,y_pred_gini)*100))
kfold = model_selection.KFold(n_splits=10, random_state=7)
modelCV = DecisionTreeClassifier(criterion = "gini", random_state = 100, max_depth=3, min_samples_leaf=5)
results = model_selection.cross_val_score(modelCV, X_train, y_train, cv=kfold, scoring='accuracy')
print("b. Accuracy for 10-fold cross validation: %.3f" % (results.mean()*100))
print("c. Confusion matrix:")
print(confusion_matrix(y_test, y_pred_gini))
print("d. Classification Report")
print(classification_report(y_test, y_pred_gini))


# #### 3. Decision Tree- Entropy

# In[45]:


print("3. Decision Tree- Entropy")
y_pred_en = entropy.predict(X_test)
y_prob_en= entropy.predict_proba(X_test)
print('a. Accuracy: {:.3f}'.format(accuracy_score(y_test,y_pred_en)*100))
kfold = model_selection.KFold(n_splits=10, random_state=7)
modelCV = DecisionTreeClassifier(criterion = "entropy", random_state = 100,max_depth=3, min_samples_leaf=5)
results = model_selection.cross_val_score(modelCV, X_train, y_train, cv=kfold, scoring='accuracy')
print("b. Accuracy for 10-fold cross validation: %.3f" % (results.mean()*100))
print("c. Confusion matrix:")
print(confusion_matrix(y_test, y_pred_en))
print("d. Classification Report")
print(classification_report(y_test, y_pred_en))


# #### 4. Neural Network

# In[46]:


print("4. Neural Network")
y_pred_NN = NN.predict(X_std_test)
y_prob_NN= NN.predict_proba(X_std_test)
print('a. Accuracy: {:.3f}'.format(accuracy_score(y_std_test,y_pred_NN)*100))
kfold = model_selection.KFold(n_splits=10, random_state=7)
modelCV = MLPClassifier(hidden_layer_sizes=(10,10,10))
results = model_selection.cross_val_score(modelCV, X_std_train, y_std_train, cv=kfold, scoring='accuracy')
print("b. Accuracy for 10-fold cross validation: %.3f" % (results.mean()*100))
print("c. Confusion matrix:")
print(confusion_matrix(y_std_test, y_pred_NN))
print("d. Classification Report")
print(classification_report(y_std_test, y_pred_NN))


# #### 5. Random Forest

# In[47]:


print("5. Random Forest")
y_pred_RF = RF.predict(X_test)
y_prob_RF= RF.predict_proba(X_test)
print('a. Accuracy: {:.3f}'.format(accuracy_score(y_test,y_pred_RF)*100))
kfold = model_selection.KFold(n_splits=10, random_state=7)
modelCV = RandomForestClassifier(n_estimators=100, oob_score=True, random_state=0)
results = model_selection.cross_val_score(modelCV, X_train, y_train, cv=kfold, scoring='accuracy')
print("b. Accuracy for 10-fold cross validation: %.3f" % (results.mean()*100))
print("c. Confusion matrix:")
print(confusion_matrix(y_test, y_pred_RF))
print("d. Classification Report")
print(classification_report(y_test, y_pred_RF))


# #### 6. ROC Curve

# In[48]:


get_ipython().run_line_magic('matplotlib', 'inline')

plt.gcf().set_size_inches(8,8)

fpr_log, tpr_log, thresholds_log = metrics.roc_curve(y_test, logreg.predict_proba(X_test)[:,1])
plt.plot(fpr_log, tpr_log, label='Logistic Regression (area = %0.3f)' % roc_auc_score(y_test, logreg.predict_proba(X_test)[:,1]))

y_prob_gini= gini.predict_proba(X_test)
fpr_gini, tpr_gini, thresholds_gini = metrics.roc_curve(y_test, y_prob_gini[:,1])
plt.plot(fpr_gini, tpr_gini, label='Decision Tree- Gini Index (area = %0.3f)' % roc_auc_score(y_test, y_prob_gini[:,1]))

y_prob_en= entropy.predict_proba(X_test)
fpr_en, tpr_en, thresholds_en = metrics.roc_curve(y_test, y_prob_en[:,1])
plt.plot(fpr_en, tpr_en, label='Decision Tree- Infomation Gain (area = %0.3f)' % roc_auc_score(y_test, y_prob_en[:,1]))

y_prob_NN= NN.predict_proba(X_std_test)
fpr_NN, tpr_NN, thresholds_NN = metrics.roc_curve(y_std_test, y_prob_NN[:,1])
plt.plot(fpr_NN, tpr_NN, label='Neural Network (area = %0.3f)' % roc_auc_score(y_std_test, y_prob_NN[:,1]))

y_prob_RF= RF.predict_proba(X_test)
fpr_RF, tpr_RF, thresholds_RF = metrics.roc_curve(y_test, y_prob_RF[:,1])
plt.plot(fpr_RF, tpr_RF, label='Random Forest (area = %0.3f)' % roc_auc_score(y_test, y_prob_RF[:,1]))

plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.savefig('ROC')
plt.show()


# ### H. Analysis
# 
# ##### 1. Model building
# - We are building a classification model where we try to predict a outcome from given information. In this case, we are given many demographic and transaction data, and we will find a relationship between those features and probability of particular outcome (Fraud or Valid transaction). 
# - To build a classification model, we have many methods such as Logistic Regression, Decision Tree, Neural Network or Random Forest. But in general, all the techniques will give you the probability of particular outcome from given data. 
# - To access which on is the best model to predict, we base on many criteria such as the nature of data, the model accuracy, confusion matrix, ROC curve.
# ##### 2. Model accuracy
# - Accuracy is a measurement of correct predictions made by the model or the ratio of fraud transactions classified as fraudulent transaction and valid classified as non-fraud to the total transactions in the test data.
# - The most accuracy model is Random Forest with 95.27% accurate. and the lowest is logistic regression with around 90.79%. 
# - The accuracy of logistics regression is only about the same as the base line of the model (90.63%)
# - We double check by 10-fold cross validation to make sure the accuracy is consistent and the train- test set division is correct. 
# - Because of the imbalance in data so the accuracy is not the correct criteria to evaluate the models.
# ##### 3. Confusion Matrix
# 1. Logistic Regression
#  False Positive: 0
#  False Negative: 4176
# 2. Decision Tree
#  False Positive: 373
#  False Negative: 1873
# 3. Neural Network:
#  False Positive: 383
#  False Negative: 1964
# 4. Random Forest 
#  False Positive: 267
#  False Negative: 1877
#  
# - When a transaction is predicted as fraud but it is actually valid, we have a false positive. On the other hand, when a transaction is predicted as valid and it is actually a fraud, we have a false negative.
# - We expect both  False Positive and False Negative are low so we get the high accuracy but we not expect the zero for False Positive in case of Logictic regression. In this case, we allow 4176 cases of fraud transaction and this will cost us a lot.
# - In case of three other model, the number of fraud transaction that allow into the system is smaller around 1800-1900 cases. Those cases are our lost. In other hand, we also wrongly predict 260-380 customers as fraud transaction but they are really not. With False Positive, we actually don't get money from customer but not lose any money to them. 
# - Considered about the cost, the lower False negative is the better model. And in this case is the Random Forest model. We can also use Decision Tree.
# 
# ##### 4. ROC curve
# - The ROC curve recommends that Random Forest and Decision Tree are best models. 
