
### Objective
##### 1. Determine customer country based on the numeric IP address.
##### 2. Build a model to predict whether an activity is fraudulent or not
##### 3. Explain how different assumptions about the cost of false positives vs false negatives would impact the model.
##### 4. Explain the models in the most simply way
##### 5. Evaluate and choose the best fit model

### Data Sources
##### 1. “Fraud_data”: information about the user first transaction. 

Columns:

•	user_id: unique id of the user

•	Signup_time: the time when the user created account (GMT time)

•	purchase_time: the value of the first transaction (USD)

•	device_id: unique id by device, may have the case that 2 transactions with the same device_id.

•	source: Marketing channels (SEO, Ads, Direct)

•	browser: the browser user use to purchase

•	sex: user gender (Male/ Female)

•	age: user age

•	ip_address: user numeric ip address

•	class: idetify whether an activity was fraudulent(1) or not (0)



##### 2. “IpAddress_to_Country”: the list of ip address with its country. Each country have a range and a numeric ip address falls within the range will belong to that country

Columns:

•	lower_bound_ip_address: the lower bound numeric ip address

•	upper_bound_ip_address: the upper bound numeric ip address

•	country: corresponding country

## Index
### A. Import library, data and data exploration
### B. Data Manipulate
### C. Exploratory Data Analysis
### D. Data Preprocessing
### E. Feature Selection
### F. Predictive Models
### G. Model Evaluation
### H. Analysis

### A. Import library, data and data exploration


```python
!pip install ipython_memwatcher
!pip install graphviz
import pandas as pd
import numpy as np
from pandas import read_csv
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn import preprocessing
from sklearn.preprocessing import scale
from sklearn.preprocessing import StandardScaler

from sklearn.feature_selection import RFE
from sklearn.decomposition import PCA
from sklearn import decomposition
from sklearn.ensemble import ExtraTreesClassifier

import statsmodels.api as sm
from sklearn.linear_model import LogisticRegression

from sklearn.cross_validation import train_test_split
from sklearn import metrics
from sklearn import model_selection
from sklearn.model_selection import cross_val_score

from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree

import graphviz 
from sklearn.tree import export_graphviz

import os
os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin'
```

    Requirement already satisfied: ipython_memwatcher in c:\programdata\anaconda3\lib\site-packages (0.2.1)
    Requirement already satisfied: memory-profiler in c:\programdata\anaconda3\lib\site-packages (from ipython_memwatcher) (0.52.0)
    Requirement already satisfied: IPython>=2.1 in c:\programdata\anaconda3\lib\site-packages (from ipython_memwatcher) (6.4.0)
    Requirement already satisfied: psutil in c:\programdata\anaconda3\lib\site-packages (from memory-profiler->ipython_memwatcher) (5.4.5)
    Requirement already satisfied: pygments in c:\programdata\anaconda3\lib\site-packages (from IPython>=2.1->ipython_memwatcher) (2.2.0)
    Requirement already satisfied: pickleshare in c:\programdata\anaconda3\lib\site-packages (from IPython>=2.1->ipython_memwatcher) (0.7.4)
    Requirement already satisfied: traitlets>=4.2 in c:\programdata\anaconda3\lib\site-packages (from IPython>=2.1->ipython_memwatcher) (4.3.2)
    Requirement already satisfied: backcall in c:\programdata\anaconda3\lib\site-packages (from IPython>=2.1->ipython_memwatcher) (0.1.0)
    Requirement already satisfied: prompt-toolkit<2.0.0,>=1.0.15 in c:\programdata\anaconda3\lib\site-packages (from IPython>=2.1->ipython_memwatcher) (1.0.15)
    Requirement already satisfied: simplegeneric>0.8 in c:\programdata\anaconda3\lib\site-packages (from IPython>=2.1->ipython_memwatcher) (0.8.1)
    Requirement already satisfied: jedi>=0.10 in c:\programdata\anaconda3\lib\site-packages (from IPython>=2.1->ipython_memwatcher) (0.12.0)
    Requirement already satisfied: setuptools>=18.5 in c:\programdata\anaconda3\lib\site-packages (from IPython>=2.1->ipython_memwatcher) (39.1.0)
    Requirement already satisfied: decorator in c:\programdata\anaconda3\lib\site-packages (from IPython>=2.1->ipython_memwatcher) (4.3.0)
    Requirement already satisfied: colorama; sys_platform == "win32" in c:\programdata\anaconda3\lib\site-packages (from IPython>=2.1->ipython_memwatcher) (0.3.9)
    Requirement already satisfied: ipython_genutils in c:\programdata\anaconda3\lib\site-packages (from traitlets>=4.2->IPython>=2.1->ipython_memwatcher) (0.2.0)
    Requirement already satisfied: six in c:\programdata\anaconda3\lib\site-packages (from traitlets>=4.2->IPython>=2.1->ipython_memwatcher) (1.11.0)
    Requirement already satisfied: wcwidth in c:\programdata\anaconda3\lib\site-packages (from prompt-toolkit<2.0.0,>=1.0.15->IPython>=2.1->ipython_memwatcher) (0.1.7)
    Requirement already satisfied: parso>=0.2.0 in c:\programdata\anaconda3\lib\site-packages (from jedi>=0.10->IPython>=2.1->ipython_memwatcher) (0.2.0)
    

    distributed 1.21.8 requires msgpack, which is not installed.
    You are using pip version 10.0.1, however version 18.0 is available.
    You should consider upgrading via the 'python -m pip install --upgrade pip' command.
    

    Requirement already satisfied: graphviz in c:\programdata\anaconda3\lib\site-packages (0.8.4)
    

    distributed 1.21.8 requires msgpack, which is not installed.
    You are using pip version 10.0.1, however version 18.0 is available.
    You should consider upgrading via the 'python -m pip install --upgrade pip' command.
    C:\ProgramData\Anaconda3\lib\site-packages\sklearn\cross_validation.py:41: DeprecationWarning: This module was deprecated in version 0.18 in favor of the model_selection module into which all the refactored classes and functions are moved. Also note that the interface of the new CV iterators are different from that of this module. This module will be removed in 0.20.
      "This module will be removed in 0.20.", DeprecationWarning)
    


```python
#Import data from csv file
fraud_data= pd.read_csv('Fraud_Data.csv')
ip = pd.read_csv('IpAddress_to_Country.csv')
```


```python
pd.set_option('display.expand_frame_repr', False) #show all columns in dataset
pd.set_option('display.max_rows', 100)
pd.set_option('display.max_columns', 200)
pd.set_option('display.width', 1000)
```


```python
#Check 5 top and bottom rows
fraud_data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>user_id</th>
      <th>signup_time</th>
      <th>purchase_time</th>
      <th>purchase_value</th>
      <th>device_id</th>
      <th>source</th>
      <th>browser</th>
      <th>sex</th>
      <th>age</th>
      <th>ip_address</th>
      <th>class</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>22058</td>
      <td>2015-02-24 22:55:49</td>
      <td>2015-04-18 02:47:11</td>
      <td>34</td>
      <td>QVPSPJUOCKZAR</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>39</td>
      <td>7.327584e+08</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>333320</td>
      <td>2015-06-07 20:39:50</td>
      <td>2015-06-08 01:38:54</td>
      <td>16</td>
      <td>EOGFQPIZPYXFZ</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>F</td>
      <td>53</td>
      <td>3.503114e+08</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1359</td>
      <td>2015-01-01 18:52:44</td>
      <td>2015-01-01 18:52:45</td>
      <td>15</td>
      <td>YSSKYOSJHPPLJ</td>
      <td>SEO</td>
      <td>Opera</td>
      <td>M</td>
      <td>53</td>
      <td>2.621474e+09</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>150084</td>
      <td>2015-04-28 21:13:25</td>
      <td>2015-05-04 13:54:50</td>
      <td>44</td>
      <td>ATGTXKYKUDUQN</td>
      <td>SEO</td>
      <td>Safari</td>
      <td>M</td>
      <td>41</td>
      <td>3.840542e+09</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>221365</td>
      <td>2015-07-21 07:09:52</td>
      <td>2015-09-09 18:40:53</td>
      <td>39</td>
      <td>NAUITBZFJKHWW</td>
      <td>Ads</td>
      <td>Safari</td>
      <td>M</td>
      <td>45</td>
      <td>4.155831e+08</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
fraud_data.tail()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>user_id</th>
      <th>signup_time</th>
      <th>purchase_time</th>
      <th>purchase_value</th>
      <th>device_id</th>
      <th>source</th>
      <th>browser</th>
      <th>sex</th>
      <th>age</th>
      <th>ip_address</th>
      <th>class</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>151107</th>
      <td>345170</td>
      <td>2015-01-27 03:03:34</td>
      <td>2015-03-29 00:30:47</td>
      <td>43</td>
      <td>XPSKTWGPWINLR</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>28</td>
      <td>3.451155e+09</td>
      <td>1</td>
    </tr>
    <tr>
      <th>151108</th>
      <td>274471</td>
      <td>2015-05-15 17:43:29</td>
      <td>2015-05-26 12:24:39</td>
      <td>35</td>
      <td>LYSFABUCPCGBA</td>
      <td>SEO</td>
      <td>Safari</td>
      <td>M</td>
      <td>32</td>
      <td>2.439047e+09</td>
      <td>0</td>
    </tr>
    <tr>
      <th>151109</th>
      <td>368416</td>
      <td>2015-03-03 23:07:31</td>
      <td>2015-05-20 07:07:47</td>
      <td>40</td>
      <td>MEQHCSJUBRBFE</td>
      <td>SEO</td>
      <td>IE</td>
      <td>F</td>
      <td>26</td>
      <td>2.748471e+09</td>
      <td>0</td>
    </tr>
    <tr>
      <th>151110</th>
      <td>207709</td>
      <td>2015-07-09 20:06:07</td>
      <td>2015-09-07 09:34:46</td>
      <td>46</td>
      <td>CMCXFGRHYSTVJ</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>37</td>
      <td>3.601175e+09</td>
      <td>0</td>
    </tr>
    <tr>
      <th>151111</th>
      <td>138208</td>
      <td>2015-06-10 07:02:20</td>
      <td>2015-07-21 02:03:53</td>
      <td>20</td>
      <td>ZINIADFCLHYPG</td>
      <td>Direct</td>
      <td>IE</td>
      <td>M</td>
      <td>38</td>
      <td>4.103825e+09</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
#Check data infomation
fraud_data.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 151112 entries, 0 to 151111
    Data columns (total 11 columns):
    user_id           151112 non-null int64
    signup_time       151112 non-null object
    purchase_time     151112 non-null object
    purchase_value    151112 non-null int64
    device_id         151112 non-null object
    source            151112 non-null object
    browser           151112 non-null object
    sex               151112 non-null object
    age               151112 non-null int64
    ip_address        151112 non-null float64
    class             151112 non-null int64
    dtypes: float64(1), int64(4), object(6)
    memory usage: 12.7+ MB
    


```python
#check number of Null value
fraud_data.isnull().sum()
```




    user_id           0
    signup_time       0
    purchase_time     0
    purchase_value    0
    device_id         0
    source            0
    browser           0
    sex               0
    age               0
    ip_address        0
    class             0
    dtype: int64




```python
#Check 5 top and bottom rows
print (ip.head())
print(ip.tail())
```

       lower_bound_ip_address  upper_bound_ip_address    country
    0              16777216.0                16777471  Australia
    1              16777472.0                16777727      China
    2              16777728.0                16778239      China
    3              16778240.0                16779263  Australia
    4              16779264.0                16781311      China
            lower_bound_ip_address  upper_bound_ip_address    country
    138841            3.758092e+09              3758093311  Hong Kong
    138842            3.758093e+09              3758094335      India
    138843            3.758095e+09              3758095871      China
    138844            3.758096e+09              3758096127  Singapore
    138845            3.758096e+09              3758096383  Australia
    


```python
#Check data infomation
ip.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 138846 entries, 0 to 138845
    Data columns (total 3 columns):
    lower_bound_ip_address    138846 non-null float64
    upper_bound_ip_address    138846 non-null int64
    country                   138846 non-null object
    dtypes: float64(1), int64(1), object(1)
    memory usage: 3.2+ MB
    


```python
#check number of Null value
ip.isnull().sum()
```




    lower_bound_ip_address    0
    upper_bound_ip_address    0
    country                   0
    dtype: int64



### B.  Data Manipulate

#### 1. Find country


```python
data=fraud_data.sort_values("ip_address").reset_index(drop=True) #sort by ip_address for faster search
country=[]
min_ip=min(ip.iloc[:,0])
max_ip=max(ip.iloc[:,1])
#define a Funtion to find country code from an Ip Code
def get_ip(ip_code):
    if len(ip.loc[(ip.lower_bound_ip_address < ip_code) & (ip.upper_bound_ip_address>ip_code), 'country'])==0: #to filter the IP codes not in csv file
        return "None"
    else:
        return ip.loc[(ip.lower_bound_ip_address < ip_code) & (ip.upper_bound_ip_address>ip_code), 'country'].values[0] #return the country when Ip Code in the range from lower and upper level
# Because already sort the IP Code so gie the Ip Code lower or higher than the threshole a name of "None"
data['country'] = data.apply(lambda r: "None" if (r['ip_address'] < min_ip) or (r['ip_address'] > max_ip) else get_ip(r['ip_address']),axis=1)
```

#### 2. Calculate interday from the day of sign up and purchase day


```python
data.signup_time=data.signup_time.astype('M8[us]') # Convert to timedate type
data.purchase_time=data.purchase_time.astype('M8[us]')  # Convert to timedate type
data['Interday_signup_purchase']=data.purchase_time-data.signup_time #Calculate Interday
data['Interday_signup_purchase']=data['Interday_signup_purchase'].dt.days #Rounded to number of days
```


```python
data.sample(100) # get sample of 100 cases
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>user_id</th>
      <th>signup_time</th>
      <th>purchase_time</th>
      <th>purchase_value</th>
      <th>device_id</th>
      <th>source</th>
      <th>browser</th>
      <th>sex</th>
      <th>age</th>
      <th>ip_address</th>
      <th>class</th>
      <th>country</th>
      <th>Interday_signup_purchase</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>103457</th>
      <td>224677</td>
      <td>2015-01-01 01:35:01</td>
      <td>2015-01-17 13:29:08</td>
      <td>25</td>
      <td>INDVLGUPQNQIP</td>
      <td>Direct</td>
      <td>IE</td>
      <td>M</td>
      <td>20</td>
      <td>2.956688e+09</td>
      <td>1</td>
      <td>Russian Federation</td>
      <td>16</td>
    </tr>
    <tr>
      <th>9270</th>
      <td>313135</td>
      <td>2015-06-10 01:46:00</td>
      <td>2015-09-06 11:45:43</td>
      <td>25</td>
      <td>PVIPGSHNHWPAB</td>
      <td>SEO</td>
      <td>Opera</td>
      <td>F</td>
      <td>30</td>
      <td>2.535425e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>88</td>
    </tr>
    <tr>
      <th>102857</th>
      <td>305409</td>
      <td>2015-01-21 14:56:33</td>
      <td>2015-02-03 20:37:41</td>
      <td>24</td>
      <td>GMQFWFRBYUNBH</td>
      <td>Direct</td>
      <td>IE</td>
      <td>F</td>
      <td>33</td>
      <td>2.938884e+09</td>
      <td>0</td>
      <td>China</td>
      <td>13</td>
    </tr>
    <tr>
      <th>102442</th>
      <td>317142</td>
      <td>2015-01-07 14:55:46</td>
      <td>2015-03-18 21:36:31</td>
      <td>106</td>
      <td>JGBIZVBYYFPDE</td>
      <td>Direct</td>
      <td>Chrome</td>
      <td>F</td>
      <td>21</td>
      <td>2.926001e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>70</td>
    </tr>
    <tr>
      <th>63860</th>
      <td>281567</td>
      <td>2015-03-28 19:02:05</td>
      <td>2015-04-19 11:53:18</td>
      <td>28</td>
      <td>FUYTPFEFFFUXM</td>
      <td>SEO</td>
      <td>Safari</td>
      <td>M</td>
      <td>28</td>
      <td>1.833628e+09</td>
      <td>0</td>
      <td>United Kingdom</td>
      <td>21</td>
    </tr>
    <tr>
      <th>73216</th>
      <td>117741</td>
      <td>2015-01-09 23:53:16</td>
      <td>2015-03-15 10:48:24</td>
      <td>25</td>
      <td>FDDZPNATLHRFB</td>
      <td>SEO</td>
      <td>IE</td>
      <td>M</td>
      <td>35</td>
      <td>2.090779e+09</td>
      <td>0</td>
      <td>Hong Kong</td>
      <td>64</td>
    </tr>
    <tr>
      <th>46349</th>
      <td>97810</td>
      <td>2015-03-20 20:00:12</td>
      <td>2015-07-02 20:34:54</td>
      <td>67</td>
      <td>ZMYXTCZZOASRQ</td>
      <td>Direct</td>
      <td>Safari</td>
      <td>M</td>
      <td>20</td>
      <td>1.320211e+09</td>
      <td>0</td>
      <td>Turkey</td>
      <td>104</td>
    </tr>
    <tr>
      <th>33271</th>
      <td>214042</td>
      <td>2015-06-16 07:22:55</td>
      <td>2015-07-05 09:22:15</td>
      <td>30</td>
      <td>IVFANXHHYMNIR</td>
      <td>SEO</td>
      <td>Safari</td>
      <td>M</td>
      <td>18</td>
      <td>9.424570e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>19</td>
    </tr>
    <tr>
      <th>144949</th>
      <td>238539</td>
      <td>2015-01-30 19:48:57</td>
      <td>2015-04-16 04:51:49</td>
      <td>22</td>
      <td>PSBPWOCRUEHNK</td>
      <td>SEO</td>
      <td>IE</td>
      <td>F</td>
      <td>25</td>
      <td>4.124695e+09</td>
      <td>0</td>
      <td>None</td>
      <td>75</td>
    </tr>
    <tr>
      <th>109830</th>
      <td>348669</td>
      <td>2015-01-13 08:13:07</td>
      <td>2015-01-27 11:48:39</td>
      <td>17</td>
      <td>XPNQOUWXXOLVT</td>
      <td>SEO</td>
      <td>Safari</td>
      <td>F</td>
      <td>32</td>
      <td>3.147096e+09</td>
      <td>0</td>
      <td>Mexico</td>
      <td>14</td>
    </tr>
    <tr>
      <th>105826</th>
      <td>348961</td>
      <td>2015-02-04 09:31:28</td>
      <td>2015-05-17 23:34:36</td>
      <td>30</td>
      <td>ESQYJDCGQVXUW</td>
      <td>Ads</td>
      <td>IE</td>
      <td>F</td>
      <td>27</td>
      <td>3.020622e+09</td>
      <td>0</td>
      <td>Japan</td>
      <td>102</td>
    </tr>
    <tr>
      <th>7359</th>
      <td>68185</td>
      <td>2015-05-19 06:36:39</td>
      <td>2015-05-21 12:35:21</td>
      <td>39</td>
      <td>KXXJZYDGMHDNS</td>
      <td>Ads</td>
      <td>IE</td>
      <td>M</td>
      <td>57</td>
      <td>2.036530e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>2</td>
    </tr>
    <tr>
      <th>90260</th>
      <td>46118</td>
      <td>2015-06-15 23:11:09</td>
      <td>2015-10-03 05:03:15</td>
      <td>71</td>
      <td>PINMVERGLHZRN</td>
      <td>Ads</td>
      <td>Safari</td>
      <td>F</td>
      <td>46</td>
      <td>2.561801e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>109</td>
    </tr>
    <tr>
      <th>77105</th>
      <td>220208</td>
      <td>2015-03-28 10:36:28</td>
      <td>2015-05-15 01:50:23</td>
      <td>32</td>
      <td>ZRHKYBQGMEOWM</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>45</td>
      <td>2.198023e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>47</td>
    </tr>
    <tr>
      <th>97684</th>
      <td>339592</td>
      <td>2015-08-09 05:21:32</td>
      <td>2015-09-03 18:20:24</td>
      <td>57</td>
      <td>KRUBYVYTIVVCC</td>
      <td>Ads</td>
      <td>Safari</td>
      <td>M</td>
      <td>34</td>
      <td>2.789676e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>25</td>
    </tr>
    <tr>
      <th>108689</th>
      <td>352308</td>
      <td>2015-06-21 15:16:05</td>
      <td>2015-10-03 06:40:45</td>
      <td>28</td>
      <td>KRJFQBMPCNHTD</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>F</td>
      <td>43</td>
      <td>3.101641e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>103</td>
    </tr>
    <tr>
      <th>122586</th>
      <td>18007</td>
      <td>2015-01-29 14:55:26</td>
      <td>2015-02-05 19:13:05</td>
      <td>53</td>
      <td>GGUWPFBPKDLMO</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>44</td>
      <td>3.509625e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>7</td>
    </tr>
    <tr>
      <th>107225</th>
      <td>146472</td>
      <td>2015-03-30 00:24:14</td>
      <td>2015-05-01 03:20:20</td>
      <td>17</td>
      <td>ZCCBJKABERIKV</td>
      <td>Direct</td>
      <td>FireFox</td>
      <td>F</td>
      <td>53</td>
      <td>3.061553e+09</td>
      <td>0</td>
      <td>China</td>
      <td>32</td>
    </tr>
    <tr>
      <th>136072</th>
      <td>336069</td>
      <td>2015-06-10 02:51:14</td>
      <td>2015-06-18 23:42:19</td>
      <td>45</td>
      <td>TRBSPEFEKFVOZ</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>F</td>
      <td>32</td>
      <td>3.877061e+09</td>
      <td>0</td>
      <td>None</td>
      <td>8</td>
    </tr>
    <tr>
      <th>140131</th>
      <td>25833</td>
      <td>2015-06-16 12:11:49</td>
      <td>2015-07-28 05:20:04</td>
      <td>23</td>
      <td>LVDZKAQAWIRDD</td>
      <td>SEO</td>
      <td>FireFox</td>
      <td>M</td>
      <td>29</td>
      <td>3.993051e+09</td>
      <td>0</td>
      <td>None</td>
      <td>41</td>
    </tr>
    <tr>
      <th>30463</th>
      <td>367829</td>
      <td>2015-08-03 13:02:15</td>
      <td>2015-11-12 22:47:53</td>
      <td>53</td>
      <td>HIUOJHUDTXDHU</td>
      <td>SEO</td>
      <td>IE</td>
      <td>F</td>
      <td>24</td>
      <td>8.478847e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>101</td>
    </tr>
    <tr>
      <th>99817</th>
      <td>40510</td>
      <td>2015-05-21 06:36:00</td>
      <td>2015-09-03 09:37:18</td>
      <td>21</td>
      <td>IGLTZWVPLIDHJ</td>
      <td>Direct</td>
      <td>FireFox</td>
      <td>M</td>
      <td>41</td>
      <td>2.850974e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>105</td>
    </tr>
    <tr>
      <th>54743</th>
      <td>234919</td>
      <td>2015-04-28 14:02:31</td>
      <td>2015-05-20 14:41:52</td>
      <td>48</td>
      <td>ZDPFUBRSEURGB</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>35</td>
      <td>1.546678e+09</td>
      <td>0</td>
      <td>United Kingdom</td>
      <td>22</td>
    </tr>
    <tr>
      <th>5441</th>
      <td>374247</td>
      <td>2015-07-21 20:00:57</td>
      <td>2015-10-13 12:50:02</td>
      <td>61</td>
      <td>JAAJJDPXDUTYT</td>
      <td>Ads</td>
      <td>FireFox</td>
      <td>M</td>
      <td>33</td>
      <td>1.507964e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>83</td>
    </tr>
    <tr>
      <th>82005</th>
      <td>395079</td>
      <td>2015-06-14 14:46:44</td>
      <td>2015-09-15 20:14:17</td>
      <td>59</td>
      <td>QUVUJSKHIHUHS</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>29</td>
      <td>2.333857e+09</td>
      <td>0</td>
      <td>Germany</td>
      <td>93</td>
    </tr>
    <tr>
      <th>9105</th>
      <td>387994</td>
      <td>2015-06-26 07:35:29</td>
      <td>2015-10-23 15:21:52</td>
      <td>33</td>
      <td>YZQHXHUFUTFFA</td>
      <td>Ads</td>
      <td>FireFox</td>
      <td>M</td>
      <td>22</td>
      <td>2.489555e+08</td>
      <td>0</td>
      <td>China</td>
      <td>119</td>
    </tr>
    <tr>
      <th>5528</th>
      <td>274676</td>
      <td>2015-04-17 16:29:13</td>
      <td>2015-06-27 12:48:12</td>
      <td>53</td>
      <td>FYCGDAKAGYDLN</td>
      <td>SEO</td>
      <td>IE</td>
      <td>M</td>
      <td>30</td>
      <td>1.527074e+08</td>
      <td>1</td>
      <td>United States</td>
      <td>70</td>
    </tr>
    <tr>
      <th>32919</th>
      <td>258814</td>
      <td>2015-05-28 19:29:57</td>
      <td>2015-06-27 20:45:26</td>
      <td>40</td>
      <td>DXUMSJMXFIUTH</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>25</td>
      <td>9.335013e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>30</td>
    </tr>
    <tr>
      <th>84610</th>
      <td>60168</td>
      <td>2015-02-01 00:06:10</td>
      <td>2015-03-10 11:08:13</td>
      <td>37</td>
      <td>EKLEUMARZJMLN</td>
      <td>SEO</td>
      <td>IE</td>
      <td>F</td>
      <td>27</td>
      <td>2.405969e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>37</td>
    </tr>
    <tr>
      <th>31519</th>
      <td>157839</td>
      <td>2015-05-18 15:55:34</td>
      <td>2015-09-06 07:30:58</td>
      <td>40</td>
      <td>CRJVDLIDVCILY</td>
      <td>Ads</td>
      <td>Safari</td>
      <td>M</td>
      <td>31</td>
      <td>8.794575e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>110</td>
    </tr>
    <tr>
      <th>3122</th>
      <td>89055</td>
      <td>2015-03-11 16:57:00</td>
      <td>2015-06-25 05:02:12</td>
      <td>34</td>
      <td>OKPMSFKVYAXZE</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>35</td>
      <td>8.518041e+07</td>
      <td>0</td>
      <td>Russian Federation</td>
      <td>105</td>
    </tr>
    <tr>
      <th>77665</th>
      <td>317288</td>
      <td>2015-02-16 17:09:28</td>
      <td>2015-06-02 20:22:50</td>
      <td>48</td>
      <td>QPRQPWNULHFKU</td>
      <td>Ads</td>
      <td>IE</td>
      <td>M</td>
      <td>34</td>
      <td>2.213144e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>106</td>
    </tr>
    <tr>
      <th>25828</th>
      <td>273564</td>
      <td>2015-03-24 06:48:56</td>
      <td>2015-03-30 17:40:52</td>
      <td>53</td>
      <td>FKMTONMKELLZP</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>37</td>
      <td>7.077115e+08</td>
      <td>0</td>
      <td>Korea Republic of</td>
      <td>6</td>
    </tr>
    <tr>
      <th>70784</th>
      <td>174895</td>
      <td>2015-01-13 02:29:21</td>
      <td>2015-01-13 02:29:22</td>
      <td>54</td>
      <td>OMLCZGHJCCBDE</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>53</td>
      <td>2.023843e+09</td>
      <td>1</td>
      <td>Indonesia</td>
      <td>0</td>
    </tr>
    <tr>
      <th>59124</th>
      <td>12524</td>
      <td>2015-05-06 19:42:04</td>
      <td>2015-05-18 04:30:19</td>
      <td>15</td>
      <td>IFHARNNFWZAAX</td>
      <td>Direct</td>
      <td>FireFox</td>
      <td>F</td>
      <td>20</td>
      <td>1.667723e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>11</td>
    </tr>
    <tr>
      <th>41674</th>
      <td>222332</td>
      <td>2015-02-04 13:44:02</td>
      <td>2015-05-06 13:02:10</td>
      <td>24</td>
      <td>AXEZGNLLHEMTO</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>31</td>
      <td>1.191076e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>90</td>
    </tr>
    <tr>
      <th>52401</th>
      <td>104879</td>
      <td>2015-06-11 21:11:34</td>
      <td>2015-08-23 12:12:04</td>
      <td>37</td>
      <td>HJEAYYZZRWBNB</td>
      <td>SEO</td>
      <td>IE</td>
      <td>F</td>
      <td>29</td>
      <td>1.482433e+09</td>
      <td>0</td>
      <td>Norway</td>
      <td>72</td>
    </tr>
    <tr>
      <th>127042</th>
      <td>335963</td>
      <td>2015-02-08 02:44:07</td>
      <td>2015-05-24 17:08:09</td>
      <td>50</td>
      <td>RFDWXFMBDBTEZ</td>
      <td>SEO</td>
      <td>FireFox</td>
      <td>F</td>
      <td>32</td>
      <td>3.629560e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>105</td>
    </tr>
    <tr>
      <th>133379</th>
      <td>33716</td>
      <td>2015-08-07 22:28:29</td>
      <td>2015-09-30 00:55:04</td>
      <td>28</td>
      <td>SMJBOZSYSOGHJ</td>
      <td>Direct</td>
      <td>IE</td>
      <td>F</td>
      <td>37</td>
      <td>3.802511e+09</td>
      <td>0</td>
      <td>None</td>
      <td>53</td>
    </tr>
    <tr>
      <th>136150</th>
      <td>95789</td>
      <td>2015-02-15 14:53:45</td>
      <td>2015-03-29 05:06:54</td>
      <td>35</td>
      <td>VWXQEXOAWROBX</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>F</td>
      <td>41</td>
      <td>3.879350e+09</td>
      <td>0</td>
      <td>None</td>
      <td>41</td>
    </tr>
    <tr>
      <th>88240</th>
      <td>399404</td>
      <td>2015-06-09 06:33:55</td>
      <td>2015-09-26 16:05:53</td>
      <td>31</td>
      <td>DKVTWNGLWETWP</td>
      <td>Ads</td>
      <td>Safari</td>
      <td>M</td>
      <td>42</td>
      <td>2.504411e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>109</td>
    </tr>
    <tr>
      <th>40813</th>
      <td>199260</td>
      <td>2015-08-05 16:26:38</td>
      <td>2015-10-31 08:30:12</td>
      <td>11</td>
      <td>FGJZKPMYVSPRD</td>
      <td>Ads</td>
      <td>IE</td>
      <td>F</td>
      <td>36</td>
      <td>1.166697e+09</td>
      <td>1</td>
      <td>United States</td>
      <td>86</td>
    </tr>
    <tr>
      <th>29171</th>
      <td>40936</td>
      <td>2015-04-27 01:16:41</td>
      <td>2015-08-18 12:23:42</td>
      <td>24</td>
      <td>HBUAZGXCSLWBS</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>F</td>
      <td>31</td>
      <td>8.128313e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>113</td>
    </tr>
    <tr>
      <th>18474</th>
      <td>368997</td>
      <td>2015-02-01 19:47:10</td>
      <td>2015-03-18 20:34:32</td>
      <td>46</td>
      <td>TTEHBAPBNHZBZ</td>
      <td>Ads</td>
      <td>FireFox</td>
      <td>M</td>
      <td>46</td>
      <td>5.086337e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>45</td>
    </tr>
    <tr>
      <th>25620</th>
      <td>107182</td>
      <td>2015-04-19 12:17:36</td>
      <td>2015-06-19 13:51:18</td>
      <td>57</td>
      <td>BDBGRVCWJFQOA</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>F</td>
      <td>30</td>
      <td>7.020520e+08</td>
      <td>0</td>
      <td>Rwanda</td>
      <td>61</td>
    </tr>
    <tr>
      <th>5073</th>
      <td>283929</td>
      <td>2015-07-07 03:37:23</td>
      <td>2015-07-18 17:02:26</td>
      <td>24</td>
      <td>PHEMVPGAWEWUM</td>
      <td>Direct</td>
      <td>IE</td>
      <td>M</td>
      <td>30</td>
      <td>1.401338e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>11</td>
    </tr>
    <tr>
      <th>142232</th>
      <td>71840</td>
      <td>2015-06-25 13:02:39</td>
      <td>2015-07-29 20:08:14</td>
      <td>20</td>
      <td>SXJNWOWGYJTJU</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>27</td>
      <td>4.050061e+09</td>
      <td>0</td>
      <td>None</td>
      <td>34</td>
    </tr>
    <tr>
      <th>62067</th>
      <td>109948</td>
      <td>2015-04-06 04:06:35</td>
      <td>2015-06-14 21:21:22</td>
      <td>47</td>
      <td>BJTKWLMLUWRTA</td>
      <td>Ads</td>
      <td>IE</td>
      <td>F</td>
      <td>30</td>
      <td>1.784592e+09</td>
      <td>0</td>
      <td>China</td>
      <td>69</td>
    </tr>
    <tr>
      <th>110000</th>
      <td>325507</td>
      <td>2015-03-12 11:40:08</td>
      <td>2015-07-02 14:19:12</td>
      <td>63</td>
      <td>EBMDTXQJMRJXB</td>
      <td>SEO</td>
      <td>FireFox</td>
      <td>F</td>
      <td>35</td>
      <td>3.151194e+09</td>
      <td>0</td>
      <td>Mexico</td>
      <td>112</td>
    </tr>
    <tr>
      <th>46930</th>
      <td>194927</td>
      <td>2015-07-08 02:56:19</td>
      <td>2015-07-16 18:24:09</td>
      <td>53</td>
      <td>GCWTUNEEMPOVE</td>
      <td>SEO</td>
      <td>IE</td>
      <td>M</td>
      <td>24</td>
      <td>1.335136e+09</td>
      <td>0</td>
      <td>Spain</td>
      <td>8</td>
    </tr>
    <tr>
      <th>87588</th>
      <td>337548</td>
      <td>2015-05-25 06:05:02</td>
      <td>2015-07-15 09:24:07</td>
      <td>25</td>
      <td>ZKLDRZLBFTIRC</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>F</td>
      <td>34</td>
      <td>2.486684e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>51</td>
    </tr>
    <tr>
      <th>107372</th>
      <td>295345</td>
      <td>2015-04-12 13:40:30</td>
      <td>2015-06-22 18:00:09</td>
      <td>30</td>
      <td>FQQTVEHFNVTFO</td>
      <td>Ads</td>
      <td>FireFox</td>
      <td>F</td>
      <td>45</td>
      <td>3.065520e+09</td>
      <td>1</td>
      <td>Pakistan</td>
      <td>71</td>
    </tr>
    <tr>
      <th>86092</th>
      <td>39875</td>
      <td>2015-03-02 05:21:09</td>
      <td>2015-03-19 05:10:48</td>
      <td>16</td>
      <td>GCBVCHOAVZMHK</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>35</td>
      <td>2.446827e+09</td>
      <td>0</td>
      <td>Netherlands</td>
      <td>16</td>
    </tr>
    <tr>
      <th>138860</th>
      <td>210607</td>
      <td>2015-07-30 22:17:23</td>
      <td>2015-11-16 12:44:17</td>
      <td>23</td>
      <td>PKOZXGOFAXYSP</td>
      <td>SEO</td>
      <td>IE</td>
      <td>F</td>
      <td>31</td>
      <td>3.956467e+09</td>
      <td>0</td>
      <td>None</td>
      <td>108</td>
    </tr>
    <tr>
      <th>62246</th>
      <td>183090</td>
      <td>2015-01-11 21:02:46</td>
      <td>2015-02-13 11:01:32</td>
      <td>33</td>
      <td>XEVUPKJLUFAMP</td>
      <td>Direct</td>
      <td>Chrome</td>
      <td>M</td>
      <td>22</td>
      <td>1.790028e+09</td>
      <td>0</td>
      <td>Japan</td>
      <td>32</td>
    </tr>
    <tr>
      <th>98853</th>
      <td>204058</td>
      <td>2015-02-05 23:42:55</td>
      <td>2015-04-21 23:47:26</td>
      <td>53</td>
      <td>IHPTPDVDEHOAH</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>F</td>
      <td>24</td>
      <td>2.821938e+09</td>
      <td>1</td>
      <td>United States</td>
      <td>75</td>
    </tr>
    <tr>
      <th>73406</th>
      <td>247343</td>
      <td>2015-06-18 20:20:53</td>
      <td>2015-09-19 05:18:32</td>
      <td>91</td>
      <td>YNEHKIARWKKWI</td>
      <td>Ads</td>
      <td>IE</td>
      <td>F</td>
      <td>34</td>
      <td>2.095073e+09</td>
      <td>0</td>
      <td>China</td>
      <td>92</td>
    </tr>
    <tr>
      <th>113370</th>
      <td>152227</td>
      <td>2015-02-03 14:16:24</td>
      <td>2015-04-30 15:06:01</td>
      <td>49</td>
      <td>ERRJAUEVTMBNA</td>
      <td>Direct</td>
      <td>IE</td>
      <td>F</td>
      <td>35</td>
      <td>3.244318e+09</td>
      <td>0</td>
      <td>Germany</td>
      <td>86</td>
    </tr>
    <tr>
      <th>121117</th>
      <td>92533</td>
      <td>2015-04-23 17:31:52</td>
      <td>2015-04-25 21:02:15</td>
      <td>58</td>
      <td>SHZXITEIPAQGD</td>
      <td>SEO</td>
      <td>IE</td>
      <td>M</td>
      <td>27</td>
      <td>3.468557e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>2</td>
    </tr>
    <tr>
      <th>6746</th>
      <td>14451</td>
      <td>2015-06-16 16:09:51</td>
      <td>2015-06-22 01:02:11</td>
      <td>23</td>
      <td>SDDSUTRVCCNSE</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>42</td>
      <td>1.884259e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>5</td>
    </tr>
    <tr>
      <th>17715</th>
      <td>180765</td>
      <td>2015-08-13 11:25:03</td>
      <td>2015-10-10 21:22:37</td>
      <td>40</td>
      <td>TCUTKZJWOBBMS</td>
      <td>Ads</td>
      <td>Safari</td>
      <td>M</td>
      <td>34</td>
      <td>4.871831e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>58</td>
    </tr>
    <tr>
      <th>26569</th>
      <td>376886</td>
      <td>2015-01-14 18:15:33</td>
      <td>2015-03-26 01:33:01</td>
      <td>37</td>
      <td>HOUQTASVZXRWX</td>
      <td>Direct</td>
      <td>IE</td>
      <td>F</td>
      <td>37</td>
      <td>7.276402e+08</td>
      <td>0</td>
      <td>Japan</td>
      <td>70</td>
    </tr>
    <tr>
      <th>141540</th>
      <td>95460</td>
      <td>2015-03-12 07:38:21</td>
      <td>2015-04-05 12:37:18</td>
      <td>23</td>
      <td>QSZRVKVWFDOHT</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>F</td>
      <td>36</td>
      <td>4.030485e+09</td>
      <td>0</td>
      <td>None</td>
      <td>24</td>
    </tr>
    <tr>
      <th>122762</th>
      <td>142324</td>
      <td>2015-07-07 14:19:06</td>
      <td>2015-10-06 14:13:04</td>
      <td>65</td>
      <td>PEVIBKVLELRXY</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>36</td>
      <td>3.514562e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>90</td>
    </tr>
    <tr>
      <th>6859</th>
      <td>286146</td>
      <td>2015-03-07 22:21:31</td>
      <td>2015-06-28 11:00:51</td>
      <td>10</td>
      <td>GOQUJRQZXIFQU</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>36</td>
      <td>1.908613e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>112</td>
    </tr>
    <tr>
      <th>54297</th>
      <td>98832</td>
      <td>2015-02-01 21:07:54</td>
      <td>2015-03-28 06:57:13</td>
      <td>39</td>
      <td>PPIKISUJBRGCQ</td>
      <td>SEO</td>
      <td>IE</td>
      <td>M</td>
      <td>27</td>
      <td>1.535026e+09</td>
      <td>0</td>
      <td>Spain</td>
      <td>54</td>
    </tr>
    <tr>
      <th>116078</th>
      <td>157574</td>
      <td>2015-02-16 17:23:25</td>
      <td>2015-04-09 23:40:52</td>
      <td>14</td>
      <td>HQJLNAIHAPUJH</td>
      <td>Direct</td>
      <td>FireFox</td>
      <td>M</td>
      <td>35</td>
      <td>3.329768e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>52</td>
    </tr>
    <tr>
      <th>90204</th>
      <td>321410</td>
      <td>2015-01-12 18:38:19</td>
      <td>2015-05-08 10:20:01</td>
      <td>12</td>
      <td>YLOUBFBJEGCPR</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>F</td>
      <td>28</td>
      <td>2.560883e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>115</td>
    </tr>
    <tr>
      <th>43636</th>
      <td>194996</td>
      <td>2015-08-02 18:16:41</td>
      <td>2015-10-28 18:07:56</td>
      <td>52</td>
      <td>HPEFTIMKLJUEO</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>F</td>
      <td>30</td>
      <td>1.245642e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>86</td>
    </tr>
    <tr>
      <th>35483</th>
      <td>267356</td>
      <td>2015-07-18 17:18:39</td>
      <td>2015-08-04 07:59:03</td>
      <td>28</td>
      <td>WOHGHQZXUCIIQ</td>
      <td>Ads</td>
      <td>Opera</td>
      <td>F</td>
      <td>31</td>
      <td>1.020806e+09</td>
      <td>0</td>
      <td>China</td>
      <td>16</td>
    </tr>
    <tr>
      <th>62768</th>
      <td>232460</td>
      <td>2015-03-07 20:45:44</td>
      <td>2015-06-30 03:17:20</td>
      <td>43</td>
      <td>EZVKMICYGKESN</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>32</td>
      <td>1.802125e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>114</td>
    </tr>
    <tr>
      <th>71212</th>
      <td>387014</td>
      <td>2015-07-14 14:42:34</td>
      <td>2015-09-28 12:09:35</td>
      <td>72</td>
      <td>FZJZPASHSAFYN</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>46</td>
      <td>2.035127e+09</td>
      <td>0</td>
      <td>China</td>
      <td>75</td>
    </tr>
    <tr>
      <th>125540</th>
      <td>288881</td>
      <td>2015-06-19 22:32:43</td>
      <td>2015-08-29 04:55:43</td>
      <td>34</td>
      <td>BCJNWMWDQGRNX</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>24</td>
      <td>3.590120e+09</td>
      <td>0</td>
      <td>Lithuania</td>
      <td>70</td>
    </tr>
    <tr>
      <th>55758</th>
      <td>329468</td>
      <td>2015-07-20 05:52:19</td>
      <td>2015-08-30 15:47:00</td>
      <td>25</td>
      <td>XJBUCGRYCZMLN</td>
      <td>SEO</td>
      <td>IE</td>
      <td>F</td>
      <td>45</td>
      <td>1.575716e+09</td>
      <td>0</td>
      <td>Germany</td>
      <td>41</td>
    </tr>
    <tr>
      <th>114475</th>
      <td>141412</td>
      <td>2015-05-22 07:05:00</td>
      <td>2015-07-12 07:12:36</td>
      <td>36</td>
      <td>JLFLISKBZWPMV</td>
      <td>Ads</td>
      <td>IE</td>
      <td>M</td>
      <td>35</td>
      <td>3.274659e+09</td>
      <td>0</td>
      <td>Czech Republic</td>
      <td>51</td>
    </tr>
    <tr>
      <th>139802</th>
      <td>225398</td>
      <td>2015-06-27 07:22:29</td>
      <td>2015-10-13 20:11:50</td>
      <td>13</td>
      <td>MZTUPJSLGKFWW</td>
      <td>SEO</td>
      <td>FireFox</td>
      <td>M</td>
      <td>41</td>
      <td>3.983876e+09</td>
      <td>0</td>
      <td>None</td>
      <td>108</td>
    </tr>
    <tr>
      <th>92764</th>
      <td>106192</td>
      <td>2015-04-15 09:41:48</td>
      <td>2015-05-13 07:55:24</td>
      <td>71</td>
      <td>RGELTNWRUDSDN</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>45</td>
      <td>2.650848e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>27</td>
    </tr>
    <tr>
      <th>106424</th>
      <td>239110</td>
      <td>2015-03-18 13:05:15</td>
      <td>2015-06-15 04:37:43</td>
      <td>49</td>
      <td>KRBAHTJAHMSZN</td>
      <td>Direct</td>
      <td>FireFox</td>
      <td>F</td>
      <td>44</td>
      <td>3.038246e+09</td>
      <td>1</td>
      <td>Argentina</td>
      <td>88</td>
    </tr>
    <tr>
      <th>10683</th>
      <td>124460</td>
      <td>2015-05-28 08:45:28</td>
      <td>2015-07-28 07:07:19</td>
      <td>47</td>
      <td>BUKBJWKFINDPZ</td>
      <td>SEO</td>
      <td>Opera</td>
      <td>F</td>
      <td>30</td>
      <td>2.934803e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>60</td>
    </tr>
    <tr>
      <th>98421</th>
      <td>330145</td>
      <td>2015-01-12 17:01:05</td>
      <td>2015-04-21 04:42:25</td>
      <td>10</td>
      <td>BRESGHPFFQFRF</td>
      <td>Ads</td>
      <td>Opera</td>
      <td>M</td>
      <td>45</td>
      <td>2.810119e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>98</td>
    </tr>
    <tr>
      <th>72547</th>
      <td>178671</td>
      <td>2015-05-10 03:28:20</td>
      <td>2015-07-07 00:12:42</td>
      <td>28</td>
      <td>RNDMWQEBIRVFM</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>37</td>
      <td>2.072714e+09</td>
      <td>0</td>
      <td>China</td>
      <td>57</td>
    </tr>
    <tr>
      <th>44116</th>
      <td>185985</td>
      <td>2015-08-04 01:58:48</td>
      <td>2015-11-03 04:00:46</td>
      <td>73</td>
      <td>THIMKJKPYUMPK</td>
      <td>Ads</td>
      <td>FireFox</td>
      <td>M</td>
      <td>31</td>
      <td>1.258487e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>91</td>
    </tr>
    <tr>
      <th>44723</th>
      <td>304027</td>
      <td>2015-04-21 00:09:12</td>
      <td>2015-07-03 20:49:58</td>
      <td>71</td>
      <td>EMTGZOPIPKQYR</td>
      <td>Ads</td>
      <td>IE</td>
      <td>M</td>
      <td>20</td>
      <td>1.274759e+09</td>
      <td>1</td>
      <td>United States</td>
      <td>73</td>
    </tr>
    <tr>
      <th>54343</th>
      <td>24202</td>
      <td>2015-07-11 22:29:07</td>
      <td>2015-09-25 02:32:04</td>
      <td>58</td>
      <td>HYUDISQYFLSKH</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>22</td>
      <td>1.536163e+09</td>
      <td>0</td>
      <td>Germany</td>
      <td>75</td>
    </tr>
    <tr>
      <th>21527</th>
      <td>3493</td>
      <td>2015-02-15 10:20:58</td>
      <td>2015-05-27 13:25:46</td>
      <td>40</td>
      <td>QGQWISRWJIOZV</td>
      <td>Direct</td>
      <td>FireFox</td>
      <td>M</td>
      <td>32</td>
      <td>5.921005e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>101</td>
    </tr>
    <tr>
      <th>47347</th>
      <td>211576</td>
      <td>2015-02-27 18:04:07</td>
      <td>2015-05-25 04:43:27</td>
      <td>26</td>
      <td>AADGMPYOPXXCX</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>37</td>
      <td>1.346017e+09</td>
      <td>1</td>
      <td>Spain</td>
      <td>86</td>
    </tr>
    <tr>
      <th>93550</th>
      <td>166075</td>
      <td>2015-03-15 22:57:57</td>
      <td>2015-03-28 00:39:12</td>
      <td>36</td>
      <td>EBYVFFJMLXQEY</td>
      <td>SEO</td>
      <td>Opera</td>
      <td>F</td>
      <td>26</td>
      <td>2.672968e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>12</td>
    </tr>
    <tr>
      <th>16582</th>
      <td>285146</td>
      <td>2015-08-12 12:27:18</td>
      <td>2015-08-17 20:22:45</td>
      <td>37</td>
      <td>QDTSDAVNJHWXY</td>
      <td>Ads</td>
      <td>FireFox</td>
      <td>M</td>
      <td>51</td>
      <td>4.567512e+08</td>
      <td>0</td>
      <td>India</td>
      <td>5</td>
    </tr>
    <tr>
      <th>66160</th>
      <td>259862</td>
      <td>2015-03-08 20:54:48</td>
      <td>2015-04-20 15:50:18</td>
      <td>28</td>
      <td>NQXCHKUOPNMWA</td>
      <td>Ads</td>
      <td>Chrome</td>
      <td>M</td>
      <td>20</td>
      <td>1.897846e+09</td>
      <td>0</td>
      <td>India</td>
      <td>42</td>
    </tr>
    <tr>
      <th>150485</th>
      <td>16228</td>
      <td>2015-05-03 21:46:32</td>
      <td>2015-05-22 06:51:35</td>
      <td>35</td>
      <td>KAGRABBNCYIGD</td>
      <td>Ads</td>
      <td>Safari</td>
      <td>M</td>
      <td>33</td>
      <td>4.276650e+09</td>
      <td>0</td>
      <td>None</td>
      <td>18</td>
    </tr>
    <tr>
      <th>145994</th>
      <td>394697</td>
      <td>2015-03-03 11:03:57</td>
      <td>2015-04-16 16:40:54</td>
      <td>31</td>
      <td>CEWQIDILXZJGL</td>
      <td>Ads</td>
      <td>IE</td>
      <td>F</td>
      <td>43</td>
      <td>4.152092e+09</td>
      <td>0</td>
      <td>None</td>
      <td>44</td>
    </tr>
    <tr>
      <th>79558</th>
      <td>34928</td>
      <td>2015-08-04 11:52:31</td>
      <td>2015-10-17 02:59:08</td>
      <td>25</td>
      <td>FBYXNNEZBVZLS</td>
      <td>Direct</td>
      <td>FireFox</td>
      <td>M</td>
      <td>39</td>
      <td>2.264549e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>73</td>
    </tr>
    <tr>
      <th>136117</th>
      <td>300146</td>
      <td>2015-03-14 12:59:48</td>
      <td>2015-05-26 15:10:12</td>
      <td>20</td>
      <td>GVMWHZJAOZGHX</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>31</td>
      <td>3.878464e+09</td>
      <td>0</td>
      <td>None</td>
      <td>73</td>
    </tr>
    <tr>
      <th>21053</th>
      <td>296231</td>
      <td>2015-08-15 18:52:19</td>
      <td>2015-09-10 07:10:42</td>
      <td>28</td>
      <td>QYXCLAYWVXXOV</td>
      <td>Ads</td>
      <td>FireFox</td>
      <td>F</td>
      <td>18</td>
      <td>5.793114e+08</td>
      <td>0</td>
      <td>United States</td>
      <td>25</td>
    </tr>
    <tr>
      <th>76184</th>
      <td>283304</td>
      <td>2015-05-30 03:12:48</td>
      <td>2015-06-01 14:34:03</td>
      <td>32</td>
      <td>DGZPJGOHYJZZI</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>38</td>
      <td>2.172251e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>2</td>
    </tr>
    <tr>
      <th>63669</th>
      <td>189456</td>
      <td>2015-04-18 08:45:20</td>
      <td>2015-05-02 09:35:08</td>
      <td>48</td>
      <td>EPNVOBVNEXWAF</td>
      <td>Direct</td>
      <td>Chrome</td>
      <td>M</td>
      <td>26</td>
      <td>1.828934e+09</td>
      <td>0</td>
      <td>France</td>
      <td>14</td>
    </tr>
    <tr>
      <th>39307</th>
      <td>386274</td>
      <td>2015-07-07 01:23:56</td>
      <td>2015-09-21 12:33:18</td>
      <td>22</td>
      <td>FNHKOGZATPSDS</td>
      <td>SEO</td>
      <td>Chrome</td>
      <td>M</td>
      <td>39</td>
      <td>1.126682e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>76</td>
    </tr>
    <tr>
      <th>145453</th>
      <td>67716</td>
      <td>2015-06-08 19:40:33</td>
      <td>2015-06-19 23:09:55</td>
      <td>43</td>
      <td>QUIEGNFJBEDCJ</td>
      <td>SEO</td>
      <td>IE</td>
      <td>M</td>
      <td>37</td>
      <td>4.138385e+09</td>
      <td>0</td>
      <td>None</td>
      <td>11</td>
    </tr>
    <tr>
      <th>84574</th>
      <td>310279</td>
      <td>2015-04-22 11:30:32</td>
      <td>2015-05-22 00:00:43</td>
      <td>15</td>
      <td>NTEVXPNDPXOUH</td>
      <td>Direct</td>
      <td>FireFox</td>
      <td>M</td>
      <td>30</td>
      <td>2.404596e+09</td>
      <td>0</td>
      <td>United States</td>
      <td>29</td>
    </tr>
    <tr>
      <th>107553</th>
      <td>110545</td>
      <td>2015-02-02 09:03:42</td>
      <td>2015-02-07 19:44:07</td>
      <td>20</td>
      <td>XMLGRZQZXYXCN</td>
      <td>Ads</td>
      <td>Safari</td>
      <td>F</td>
      <td>36</td>
      <td>3.070333e+09</td>
      <td>0</td>
      <td>China</td>
      <td>5</td>
    </tr>
  </tbody>
</table>
</div>



### C. Exploratory Data Analysis

#### 1. Class


```python
print(data['class'].value_counts())
sns.countplot(x='class',data=data)
```

    0    136961
    1     14151
    Name: class, dtype: int64
    




    <matplotlib.axes._subplots.AxesSubplot at 0x1e32f16b940>




![png](output_22_2.png)



```python
data.groupby('class').mean()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>user_id</th>
      <th>purchase_value</th>
      <th>age</th>
      <th>ip_address</th>
      <th>Interday_signup_purchase</th>
    </tr>
    <tr>
      <th>class</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>200098.920123</td>
      <td>36.929418</td>
      <td>33.122356</td>
      <td>2.154235e+09</td>
      <td>59.584152</td>
    </tr>
    <tr>
      <th>1</th>
      <td>200869.065366</td>
      <td>36.993004</td>
      <td>33.318281</td>
      <td>2.131918e+09</td>
      <td>27.822415</td>
    </tr>
  </tbody>
</table>
</div>



##### Comment:
- The fraudulent cases is 9.4% total cases. This cause an imbalance in our data. In our model, we will consider using multiple models and performance metrics to cope with the imbalance data.
- The average of purchase value and age of those 2 classes are quite similar but the average interday for fraudulent transactions are much smaller than valid ones. This suggests that interday will be one of the most important variables to predict fraud.

#### 2. Class and Source


```python
plt.gcf().set_size_inches(16,8)
table=pd.crosstab(data.source,data['class'])
table.plot(kind='bar',ax=plt.subplot(221))
plt.title('Fraud vs Source')
plt.xlabel('Source')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend
table.div(table.sum(1).astype(float), axis=0).plot(kind='bar',legend=None, stacked=True, ax=plt.subplot(222))
plt.title('Stacked Bar Chart of Fraud vs Source')
plt.xlabel('Source')
plt.ylabel('Proportion of Fraud')
```




    Text(0,0.5,'Proportion of Fraud')




![png](output_26_1.png)


##### Comment:
- Transactions come mostly from Ads and SEO.
- Proportions of fraud for 3 sources are quite similar but ratio for Direct transactions is a little bit higher

#### 3. Fraud class and Browser


```python
plt.gcf().set_size_inches(16,8)
table=pd.crosstab(data.browser,data['class'])
table.plot(kind='bar',ax=plt.subplot(221))
plt.title('Fraud vs Browser')
plt.xlabel('Browser')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend
table.div(table.sum(1).astype(float), axis=0).plot(kind='bar',legend=None, stacked=True, ax=plt.subplot(222))
plt.title('Stacked Bar Chart of Fraud vs Browser')
plt.xlabel('Browser')
plt.ylabel('Proportion of Fraud')
```




    Text(0,0.5,'Proportion of Fraud')




![png](output_29_1.png)


##### Comment:
- Chrome is the most popular browser and Internet Explorer is in the second place. 
- The proportions of fraud are in the same level between browsers but Chrome and Firefox are slightly bigger

#### 4. Fraud and Gender


```python
plt.gcf().set_size_inches(16,8)
table=pd.crosstab(data.sex,data['class'])
table.plot(kind='bar',ax=plt.subplot(221))
plt.title('Fraud vs Gender')
plt.xlabel('Gender')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend
table.div(table.sum(1).astype(float), axis=0).plot(kind='bar', legend=None, stacked=True, ax=plt.subplot(222))
plt.title('Stacked Bar Chart of Fraud vs Gender')
plt.xlabel('Gender')
plt.ylabel('Proportion of Fraud')
```




    Text(0,0.5,'Proportion of Fraud')




![png](output_32_1.png)


##### Comment:
- More male customers than female
- Transactions from male customers also at a bit higher risk than female

#### 5. Fraud and Age


```python
print ("Average age: ", data.age.mean())
print ("Median age: ", data.age.median())
```

    Average age:  33.14070358409656
    Median age:  33.0
    


```python
plt.gcf().set_size_inches(12,12)
table=pd.crosstab(data.age,data['class'])
table.plot(kind='bar',ax=plt.subplot(211))
plt.title('Fraud vs Age')
plt.xlabel('Age')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend

table.div(table.sum(1).astype(float), axis=0).plot(kind='bar',legend=None, stacked=True, ax=plt.subplot(212))
plt.title('Stacked Bar Chart of Fraud vs Age')
plt.xlabel('Age')
plt.ylabel('Proportion of Fraud')
```




    Text(0,0.5,'Proportion of Fraud')




![png](output_36_1.png)


##### Comment:
- Customer age ranges from 18 to 76.
- Average age is 33.14 and Median age:  33.0
- Higher rate of fraud is from group of 63 y.o where around 30% transaction are fraudulent. Second risk group is 61 y.o with around 20% fraud

#### 6. Fraud and Purchase value


```python
print ("Average purchase value: ", data.purchase_value.mean())
print ("Median purchase value: ", data.purchase_value.median())
```

    Average purchase value:  36.93537243898565
    Median purchase value:  35.0
    


```python
plt.gcf().set_size_inches(18,12)
table=pd.crosstab(data.purchase_value,data['class'])
table.plot(kind='bar',ax=plt.subplot(211))
plt.title('Fraud vs Purchase_value')
plt.xlabel('Purchase_value')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend

table.div(table.sum(1).astype(float), axis=0).plot(kind='bar',legend=None, stacked=True, ax=plt.subplot(212))
plt.title('Stacked Bar Chart of Fraud vs Purchase_value')
plt.xlabel('Purchase_value')
plt.ylabel('Proportion of Fraud')
```




    Text(0,0.5,'Proportion of Fraud')




![png](output_40_1.png)


##### Comment:
- Lowest purchase value is 9 and the highest is 154
- Average purchase value is 36.94 and Median purchase value is 35.0
- 3 most fraudulent transactions are with values of 100, 88 and 92 (25-30% of fraud)

#### 7. Fraud and Country


```python
plt.gcf().set_size_inches(16,10)
table=data.pivot_table(["user_id"],index='country',columns="class",aggfunc="count", margins=True)
table1=table.sort_values(by=('user_id','All'), ascending=False).iloc[1:11,0:2]
table1.plot(kind='barh', stacked=False, ax=plt.subplot(221))
plt.title('Top 10 Countries in number of customer')
plt.xlabel('Country')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend
table2=table.div(table.sum(1).astype(float), axis=0).sort_values(by=('user_id',1), ascending=False).iloc[1:11,0:2]
table2.plot(kind='barh', legend=None, stacked=True, ax=plt.subplot(222))
plt.title('Stacked Bar Chart of Fraud in top 10 countries')
plt.xlabel('Country')
plt.ylabel('Proportion of Fraud')
```




    Text(0,0.5,'Proportion of Fraud')




![png](output_43_1.png)


##### Comment:
- Nearly 1/3 of the transactions are from United States. In second is the Non-identified IP address which is marked as "None" for country. So, the number of fraudulent transaction is also the highest.
- In term of fraud rate, transactions from Namibia when nearly 50% of transactions are invalid. Second is Sri Lanka and surprisingly Luxembourg in third place. 


#### 8. Fraud and Days from sign up


```python
plt.gcf().set_size_inches(18,12)
table=pd.crosstab(data.Interday_signup_purchase,data['class'])
table.plot(kind='bar',ax=plt.subplot(211))
plt.title('Fraud vs Interday from sign up')
plt.xlabel('Interday from sign up')
plt.legend(labels=['Valid','Fraudulent'], loc=1, fontsize=10) #show graph's legend

table.div(table.sum(1).astype(float), axis=0).plot(kind='bar',legend=None, stacked=True, ax=plt.subplot(212))
plt.title('Stacked Bar Chart of Fraud vs Interday from sign up')
plt.xlabel('Interday from sign up')
plt.ylabel('Proportion of Fraud')
```




    Text(0,0.5,'Proportion of Fraud')




![png](output_46_1.png)


##### Comment:
- The distribution of interday is quite balance in ranges. But the number of fraudulent cases fro 0 interday is significantly high with nearly 8000 cases. As we mentioned above, interday is a very important predictor.
- In very similar way, The rate of fraud for 0 interday is the highest at around 90%

### D. Data Preprocessing

#### 1. Encoding categorical variables


```python
#source Ads = 0, Direct =1, SEO =2
le = preprocessing.LabelEncoder()
le.fit(data.source)
print (list(le.classes_))
data['source_code']=le.transform(data.source)
```

    ['Ads', 'Direct', 'SEO']
    


```python
#browser 'Chrome'=0, 'FireFox'=1, 'IE'=2, 'Opera'=3, 'Safari'=4
le = preprocessing.LabelEncoder()
le.fit(data.browser)
print (list(le.classes_))
data['browser_code']=le.transform(data.browser)
```

    ['Chrome', 'FireFox', 'IE', 'Opera', 'Safari']
    


```python
#sex Female= 0, Male =1
le = preprocessing.LabelEncoder()
le.fit(data.sex)
print (list(le.classes_))
data['sex_code']=le.transform(data.sex)
```

    ['F', 'M']
    


```python
data.country.nunique()
```




    182




```python
#There are 182 countries in our data, each country will receive a code
le = preprocessing.LabelEncoder()
le.fit(data.country)
print (list(le.classes_))
data['country_code']=le.transform(data.country)
```

    ['Afghanistan', 'Albania', 'Algeria', 'Angola', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bonaire; Sint Eustatius; Saba', 'Bosnia and Herzegowina', 'Botswana', 'Brazil', 'British Indian Ocean Territory', 'Brunei Darussalam', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Cayman Islands', 'Chile', 'China', 'Colombia', 'Congo', 'Congo The Democratic Republic of The', 'Costa Rica', "Cote D'ivoire", 'Croatia (LOCAL Name: Hrvatska)', 'Cuba', 'Curacao', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'Ecuador', 'Egypt', 'El Salvador', 'Estonia', 'Ethiopia', 'European Union', 'Faroe Islands', 'Fiji', 'Finland', 'France', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Guadeloupe', 'Guam', 'Guatemala', 'Haiti', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran (ISLAMIC Republic Of)', 'Iraq', 'Ireland', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Korea Republic of', 'Kuwait', 'Kyrgyzstan', "Lao People's Democratic Republic", 'Latvia', 'Lebanon', 'Lesotho', 'Libyan Arab Jamahiriya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macau', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Malta', 'Mauritius', 'Mexico', 'Moldova Republic of', 'Monaco', 'Mongolia', 'Montenegro', 'Morocco', 'Mozambique', 'Myanmar', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'None', 'Norway', 'Oman', 'Pakistan', 'Palestinian Territory Occupied', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Reunion', 'Romania', 'Russian Federation', 'Rwanda', 'Saint Kitts and Nevis', 'Saint Martin', 'San Marino', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Singapore', 'Slovakia (SLOVAK Republic)', 'Slovenia', 'South Africa', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Sweden', 'Switzerland', 'Syrian Arab Republic', 'Taiwan; Republic of China (ROC)', 'Tajikistan', 'Tanzania United Republic of', 'Thailand', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Venezuela', 'Viet Nam', 'Virgin Islands (U.S.)', 'Yemen', 'Zambia', 'Zimbabwe']
    


```python
data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>user_id</th>
      <th>signup_time</th>
      <th>purchase_time</th>
      <th>purchase_value</th>
      <th>device_id</th>
      <th>source</th>
      <th>browser</th>
      <th>sex</th>
      <th>age</th>
      <th>ip_address</th>
      <th>class</th>
      <th>country</th>
      <th>Interday_signup_purchase</th>
      <th>source_code</th>
      <th>browser_code</th>
      <th>sex_code</th>
      <th>country_code</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>62421</td>
      <td>2015-02-16 00:17:05</td>
      <td>2015-03-08 10:00:39</td>
      <td>46</td>
      <td>ZCLZTAJPCRAQX</td>
      <td>Direct</td>
      <td>Safari</td>
      <td>M</td>
      <td>36</td>
      <td>52093.496895</td>
      <td>0</td>
      <td>None</td>
      <td>20</td>
      <td>1</td>
      <td>4</td>
      <td>1</td>
      <td>124</td>
    </tr>
    <tr>
      <th>1</th>
      <td>173212</td>
      <td>2015-03-08 04:03:22</td>
      <td>2015-03-20 17:23:45</td>
      <td>33</td>
      <td>YFGYOALADBHLT</td>
      <td>Ads</td>
      <td>IE</td>
      <td>F</td>
      <td>30</td>
      <td>93447.138961</td>
      <td>0</td>
      <td>None</td>
      <td>12</td>
      <td>0</td>
      <td>2</td>
      <td>0</td>
      <td>124</td>
    </tr>
    <tr>
      <th>2</th>
      <td>242286</td>
      <td>2015-05-17 16:45:54</td>
      <td>2015-05-26 08:54:34</td>
      <td>33</td>
      <td>QZNVQTUITFTHH</td>
      <td>Direct</td>
      <td>FireFox</td>
      <td>F</td>
      <td>32</td>
      <td>105818.501505</td>
      <td>0</td>
      <td>None</td>
      <td>8</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>124</td>
    </tr>
    <tr>
      <th>3</th>
      <td>370003</td>
      <td>2015-03-03 19:58:39</td>
      <td>2015-05-28 21:09:13</td>
      <td>33</td>
      <td>PIBUQMBIELMMG</td>
      <td>Ads</td>
      <td>IE</td>
      <td>M</td>
      <td>40</td>
      <td>117566.664867</td>
      <td>0</td>
      <td>None</td>
      <td>86</td>
      <td>0</td>
      <td>2</td>
      <td>1</td>
      <td>124</td>
    </tr>
    <tr>
      <th>4</th>
      <td>119824</td>
      <td>2015-03-20 00:31:27</td>
      <td>2015-04-05 07:31:46</td>
      <td>55</td>
      <td>WFIIFCPIOGMHT</td>
      <td>Ads</td>
      <td>Safari</td>
      <td>M</td>
      <td>38</td>
      <td>131423.789042</td>
      <td>0</td>
      <td>None</td>
      <td>16</td>
      <td>0</td>
      <td>4</td>
      <td>1</td>
      <td>124</td>
    </tr>
  </tbody>
</table>
</div>



### E. Feature Selection


```python
data.columns
```




    Index(['user_id', 'signup_time', 'purchase_time', 'purchase_value', 'device_id', 'source', 'browser', 'sex', 'age', 'ip_address', 'class', 'country', 'Interday_signup_purchase', 'source_code', 'browser_code', 'sex_code', 'country_code'], dtype='object')




```python
Y=data['class'] #Y: Target
X=data.drop(['user_id','class','signup_time', 'purchase_time', 'device_id', 'source', 'browser', 'sex', 'ip_address', 'country'], axis=1)
#X: Predictor
```

##### Comment
- We will only use the variables with numeric values in our models so I only select those variables. 
- In this step I seperate 

#### 1. Pearson Correlation


```python
# calculate the correlation matrix
corr = X.corr()
print(corr)
# plot the heatmap
sns.heatmap(corr, xticklabels=corr.columns, yticklabels=corr.columns)
```

                              purchase_value       age  Interday_signup_purchase  source_code  browser_code  sex_code  country_code
    purchase_value                  1.000000  0.002370                  0.003422     0.000417     -0.001727  0.001996     -0.003736
    age                             0.002370  1.000000                 -0.000570    -0.000875      0.002176  0.004093      0.004073
    Interday_signup_purchase        0.003422 -0.000570                  1.000000     0.003772      0.013028 -0.005210     -0.000380
    source_code                     0.000417 -0.000875                  0.003772     1.000000      0.004397  0.001025     -0.001991
    browser_code                   -0.001727  0.002176                  0.013028     0.004397      1.000000  0.000002      0.000742
    sex_code                        0.001996  0.004093                 -0.005210     0.001025      0.000002  1.000000     -0.001873
    country_code                   -0.003736  0.004073                 -0.000380    -0.001991      0.000742 -0.001873      1.000000
    




    <matplotlib.axes._subplots.AxesSubplot at 0x1e32e444d30>




![png](output_61_2.png)


##### Comment
- There is no high correlated between features. So we can use all the variables.

#### 2. Feature selection:


```python
model = LogisticRegression()
rfe = RFE(model, 7)
fit = rfe.fit(X,Y)
print(("Num Features: %d") % fit.n_features_)

print("Selected Features:")
print(pd.DataFrame(list(zip(X.columns.tolist(), fit.support_)),columns=["Var","Selection"]).set_index("Var").iloc[:,0:2])
print("Feature Ranking:")
print(pd.DataFrame(list(zip(X.columns.tolist(), fit.ranking_)),columns=["Var","Rank"]).set_index("Var").iloc[:,0:2])
```

    Num Features: 7
    Selected Features:
                              Selection
    Var                                
    purchase_value                 True
    age                            True
    Interday_signup_purchase       True
    source_code                    True
    browser_code                   True
    sex_code                       True
    country_code                   True
    Feature Ranking:
                              Rank
    Var                           
    purchase_value               1
    age                          1
    Interday_signup_purchase     1
    source_code                  1
    browser_code                 1
    sex_code                     1
    country_code                 1
    

##### Comment
- We base on the Logistic regression to choose the feature by include each variable in the model and test the significance of the model. 
- All 7 variables are selected in this case and all rank the same.

#### 3. PCA


```python
X_std = StandardScaler().fit_transform(X) #Standardize all values
pca = PCA(n_components=7)
fit = pca.fit(X_std)
# summarize components
print(("Explained Variance: %s") % fit.explained_variance_ratio_)
plt.plot(np.cumsum(fit.explained_variance_ratio_), c='red',marker = ".")
plt.xticks(np.arange(0, 10,2))
plt.gcf().set_size_inches(8,4)
plt.grid()
plt.xlabel('number of components')
plt.ylabel('cumulative explained variance')
plt.show()
```

    Explained Variance: [0.1451291  0.14371653 0.14363635 0.1428955  0.14222066 0.14172441
     0.14067745]
    


![png](output_67_1.png)


##### Comment
- In PCA, we need to standardize the data 
- The variances for all 7 variables are similar so PCA can not help us to define new PCs. 

#### 4. Importance Ranking


```python
plt.gcf().set_size_inches(8,6)
model = ExtraTreesClassifier()
model.fit(X,Y)
importance= pd.DataFrame(list(zip(X.columns,model.feature_importances_)),columns=["Var","Importance"])
importance=importance.sort_values(by="Importance",ascending=False).set_index("Var")
print(importance)
importance.plot(kind='barh',legend= None)
plt.title('Variable Importance rank')
plt.xlabel('Importance')
```

                              Importance
    Var                                 
    Interday_signup_purchase    0.499284
    purchase_value              0.193858
    age                         0.161634
    country_code                0.102225
    browser_code                0.023925
    source_code                 0.013521
    sex_code                    0.005554
    




    Text(0.5,0,'Importance')




    <Figure size 576x432 with 0 Axes>



![png](output_70_3.png)


##### Comment
- Interday is the most important feature.
- Second is Purchase value and third is age. 
- Less important variable is sex_code.


##### => we include all features in the model

### F. Predictive Models

#### 1. Logistic Regression


```python
logit_model=sm.Logit(Y,X)
result=logit_model.fit()
print(result.summary())
```

    Optimization terminated successfully.
             Current function value: 0.276736
             Iterations 7
                               Logit Regression Results                           
    ==============================================================================
    Dep. Variable:                  class   No. Observations:               151112
    Model:                          Logit   Df Residuals:                   151105
    Method:                           MLE   Df Model:                            6
    Date:                Sat, 11 Aug 2018   Pseudo R-squ.:                  0.1099
    Time:                        13:15:03   Log-Likelihood:                -41818.
    converged:                       True   LL-Null:                       -46980.
                                            LLR p-value:                     0.000
    ============================================================================================
                                   coef    std err          z      P>|z|      [0.025      0.975]
    --------------------------------------------------------------------------------------------
    purchase_value              -0.0039      0.000     -8.260      0.000      -0.005      -0.003
    age                         -0.0146      0.001    -19.275      0.000      -0.016      -0.013
    Interday_signup_purchase    -0.0307      0.000    -97.259      0.000      -0.031      -0.030
    source_code                 -0.0625      0.010     -6.253      0.000      -0.082      -0.043
    browser_code                -0.0486      0.006     -7.643      0.000      -0.061      -0.036
    sex_code                    -0.0468      0.018     -2.598      0.009      -0.082      -0.011
    country_code                -0.0014      0.000     -9.468      0.000      -0.002      -0.001
    ============================================================================================
    


```python
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=0)
logreg = LogisticRegression()
logreg.fit(X_train, y_train)
```




    LogisticRegression(C=1.0, class_weight=None, dual=False, fit_intercept=True,
              intercept_scaling=1, max_iter=100, multi_class='ovr', n_jobs=1,
              penalty='l2', random_state=None, solver='liblinear', tol=0.0001,
              verbose=0, warm_start=False)



#### 2. Decision Tree - Gini Index


```python
gini = DecisionTreeClassifier(criterion = "gini", random_state = 100, max_depth=3, min_samples_leaf=5)
gini.fit(X_train, y_train)
dot_data = tree.export_graphviz(gini, out_file=None) 
graph = graphviz.Source(dot_data)
graph
```




![svg](output_77_0.svg)



#### 3. Decision Tree- Entropy


```python
entropy = DecisionTreeClassifier(criterion = "entropy", random_state = 100,max_depth=3, min_samples_leaf=5)
entropy.fit(X_train, y_train)
dot_data = tree.export_graphviz(entropy, out_file=None) 
graph = graphviz.Source(dot_data)
graph
```




![svg](output_79_0.svg)



#### 4. Neural Network


```python
X_std_train, X_std_test, y_std_train, y_std_test = train_test_split(X_std, Y, test_size=0.3, random_state=0)
from sklearn.neural_network import MLPClassifier
NN = MLPClassifier(hidden_layer_sizes=(10,10,10))
NN.fit(X_std_train,y_std_train)
```




    MLPClassifier(activation='relu', alpha=0.0001, batch_size='auto', beta_1=0.9,
           beta_2=0.999, early_stopping=False, epsilon=1e-08,
           hidden_layer_sizes=(10, 10, 10), learning_rate='constant',
           learning_rate_init=0.001, max_iter=200, momentum=0.9,
           nesterovs_momentum=True, power_t=0.5, random_state=None,
           shuffle=True, solver='adam', tol=0.0001, validation_fraction=0.1,
           verbose=False, warm_start=False)



#### 5. Random Forest


```python
from sklearn.ensemble import RandomForestClassifier
RF = RandomForestClassifier(n_estimators=100, oob_score=True, random_state=0)
RF.fit(X_train, y_train)
y_pred_RF = RF.predict(X_test)
```

### G. Model Evaluation

#### 1. Logistic Regression


```python
print("1. Logistic Regression")
y_pred_log = logreg.predict(X_test)
y_prob_log = logreg.predict_proba(X_test)
print('a. Accuracy: {:.2f}'.format(logreg.score(X_test, y_test)*100))
kfold = model_selection.KFold(n_splits=10, random_state=7)
modelCV = LogisticRegression()
results = model_selection.cross_val_score(modelCV, X_train, y_train, cv=kfold, scoring='accuracy')
print("b. Accuracy for 10-fold cross validation: %.2f" % (results.mean()*100))
print("c. Confusion matrix:")
print(confusion_matrix(y_test, y_pred_log))
print("d. Classification Report")
print(classification_report(y_test, y_pred_log))

```

    1. Logistic Regression
    a. Accuracy: 90.79
    b. Accuracy for 10-fold cross validation: 90.57
    c. Confusion matrix:
    [[41158     0]
     [ 4176     0]]
    d. Classification Report
                 precision    recall  f1-score   support
    
              0       0.91      1.00      0.95     41158
              1       0.00      0.00      0.00      4176
    
    avg / total       0.82      0.91      0.86     45334
    
    

    C:\ProgramData\Anaconda3\lib\site-packages\sklearn\metrics\classification.py:1135: UndefinedMetricWarning: Precision and F-score are ill-defined and being set to 0.0 in labels with no predicted samples.
      'precision', 'predicted', average, warn_for)
    

#### 2. Decision Tree - Gini Index


```python
print("2. Decision Tree- Gini Index")
y_pred_gini = gini.predict(X_test)
y_prob_gini= gini.predict_proba(X_test)
print('a. Accuracy: {:.3f}'.format(accuracy_score(y_test,y_pred_gini)*100))
kfold = model_selection.KFold(n_splits=10, random_state=7)
modelCV = DecisionTreeClassifier(criterion = "gini", random_state = 100, max_depth=3, min_samples_leaf=5)
results = model_selection.cross_val_score(modelCV, X_train, y_train, cv=kfold, scoring='accuracy')
print("b. Accuracy for 10-fold cross validation: %.3f" % (results.mean()*100))
print("c. Confusion matrix:")
print(confusion_matrix(y_test, y_pred_gini))
print("d. Classification Report")
print(classification_report(y_test, y_pred_gini))

```

    2. Decision Tree- Gini Index
    a. Accuracy: 95.046
    b. Accuracy for 10-fold cross validation: 94.926
    c. Confusion matrix:
    [[40785   373]
     [ 1873  2303]]
    d. Classification Report
                 precision    recall  f1-score   support
    
              0       0.96      0.99      0.97     41158
              1       0.86      0.55      0.67      4176
    
    avg / total       0.95      0.95      0.95     45334
    
    

#### 3. Decision Tree- Entropy


```python
print("3. Decision Tree- Entropy")
y_pred_en = entropy.predict(X_test)
y_prob_en= entropy.predict_proba(X_test)
print('a. Accuracy: {:.3f}'.format(accuracy_score(y_test,y_pred_en)*100))
kfold = model_selection.KFold(n_splits=10, random_state=7)
modelCV = DecisionTreeClassifier(criterion = "entropy", random_state = 100,max_depth=3, min_samples_leaf=5)
results = model_selection.cross_val_score(modelCV, X_train, y_train, cv=kfold, scoring='accuracy')
print("b. Accuracy for 10-fold cross validation: %.3f" % (results.mean()*100))
print("c. Confusion matrix:")
print(confusion_matrix(y_test, y_pred_en))
print("d. Classification Report")
print(classification_report(y_test, y_pred_en))

```

    3. Decision Tree- Entropy
    a. Accuracy: 95.046
    b. Accuracy for 10-fold cross validation: 94.926
    c. Confusion matrix:
    [[40785   373]
     [ 1873  2303]]
    d. Classification Report
                 precision    recall  f1-score   support
    
              0       0.96      0.99      0.97     41158
              1       0.86      0.55      0.67      4176
    
    avg / total       0.95      0.95      0.95     45334
    
    

#### 4. Neural Network


```python
print("4. Neural Network")
y_pred_NN = NN.predict(X_std_test)
y_prob_NN= NN.predict_proba(X_std_test)
print('a. Accuracy: {:.3f}'.format(accuracy_score(y_std_test,y_pred_NN)*100))
kfold = model_selection.KFold(n_splits=10, random_state=7)
modelCV = MLPClassifier(hidden_layer_sizes=(10,10,10))
results = model_selection.cross_val_score(modelCV, X_std_train, y_std_train, cv=kfold, scoring='accuracy')
print("b. Accuracy for 10-fold cross validation: %.3f" % (results.mean()*100))
print("c. Confusion matrix:")
print(confusion_matrix(y_std_test, y_pred_NN))
print("d. Classification Report")
print(classification_report(y_std_test, y_pred_NN))
```

    4. Neural Network
    a. Accuracy: 95.041
    b. Accuracy for 10-fold cross validation: 94.758
    c. Confusion matrix:
    [[40786   372]
     [ 1876  2300]]
    d. Classification Report
                 precision    recall  f1-score   support
    
              0       0.96      0.99      0.97     41158
              1       0.86      0.55      0.67      4176
    
    avg / total       0.95      0.95      0.95     45334
    
    

#### 5. Random Forest


```python
print("5. Random Forest")
y_pred_RF = RF.predict(X_test)
y_prob_RF= RF.predict_proba(X_test)
print('a. Accuracy: {:.3f}'.format(accuracy_score(y_test,y_pred_RF)*100))
kfold = model_selection.KFold(n_splits=10, random_state=7)
modelCV = RandomForestClassifier(n_estimators=100, oob_score=True, random_state=0)
results = model_selection.cross_val_score(modelCV, X_train, y_train, cv=kfold, scoring='accuracy')
print("b. Accuracy for 10-fold cross validation: %.3f" % (results.mean()*100))
print("c. Confusion matrix:")
print(confusion_matrix(y_test, y_pred_RF))
print("d. Classification Report")
print(classification_report(y_test, y_pred_RF))
```

    5. Random Forest
    a. Accuracy: 95.271
    b. Accuracy for 10-fold cross validation: 95.073
    c. Confusion matrix:
    [[40891   267]
     [ 1877  2299]]
    d. Classification Report
                 precision    recall  f1-score   support
    
              0       0.96      0.99      0.97     41158
              1       0.90      0.55      0.68      4176
    
    avg / total       0.95      0.95      0.95     45334
    
    

#### 6. ROC Curve


```python
plt.gcf().set_size_inches(8,8)

fpr_log, tpr_log, thresholds_log = metrics.roc_curve(y_test, logreg.predict_proba(X_test)[:,1])
plt.plot(fpr_log, tpr_log, label='Logistic Regression (area = %0.3f)' % roc_auc_score(y_test, logreg.predict_proba(X_test)[:,1]))

y_prob_gini= gini.predict_proba(X_test)
fpr_gini, tpr_gini, thresholds_gini = metrics.roc_curve(y_test, y_prob_gini[:,1])
plt.plot(fpr_gini, tpr_gini, label='Decision Tree- Gini Index (area = %0.3f)' % roc_auc_score(y_test, y_prob_gini[:,1]))

y_prob_en= entropy.predict_proba(X_test)
fpr_en, tpr_en, thresholds_en = metrics.roc_curve(y_test, y_prob_en[:,1])
plt.plot(fpr_en, tpr_en, label='Decision Tree- Infomation Gain (area = %0.3f)' % roc_auc_score(y_test, y_prob_en[:,1]))

y_prob_NN= NN.predict_proba(X_std_test)
fpr_NN, tpr_NN, thresholds_NN = metrics.roc_curve(y_std_test, y_prob_NN[:,1])
plt.plot(fpr_NN, tpr_NN, label='Neural Network (area = %0.3f)' % roc_auc_score(y_std_test, y_prob_NN[:,1]))

y_prob_RF= RF.predict_proba(X_test)
fpr_RF, tpr_RF, thresholds_RF = metrics.roc_curve(y_test, y_prob_RF[:,1])
plt.plot(fpr_RF, tpr_RF, label='Random Forest (area = %0.3f)' % roc_auc_score(y_test, y_prob_RF[:,1]))

plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic')
plt.legend(loc="lower right")
plt.savefig('ROC')
plt.show()
```


![png](output_96_0.png)


### H. Analysis

##### 1. Model building
- We are building a classification model where we try to predict a outcome from given information. In this case, we are given many demographic and transaction data, and we will find a relationship between those features and probability of particular outcome (Fraud or Valid transaction). 
- To build a classification model, we have many methods such as Logistic Regression, Decision Tree, Neural Network or Random Forest. But in general, all the techniques will give you the probability of particular outcome from given data. 
- To access which on is the best model to predict, we base on many criteria such as the nature of data, the model accuracy, confusion matrix, ROC curve.
##### 2. Model accuracy
- Accuracy is a measurement of correct predictions made by the model or the ratio of fraud transactions classified as fraudulent transaction and valid classified as non-fraud to the total transactions in the test data.
- The most accuracy model is Random Forest with 95.27% accurate. and the lowest is logistic regression with around 90.79%. 
- The accuracy of logistics regression is only about the same as the base line of the model (90.63%)
- We double check by 10-fold cross validation to make sure the accuracy is consistent and the train- test set division is correct. 
- Because of the imbalance in data so the accuracy is not the correct criteria to evaluate the models.
##### 3. Confusion Matrix
1. Logistic Regression
 False Positive: 0
 False Negative: 4176
2. Decision Tree
 False Positive: 373
 False Negative: 1873
3. Neural Network:
 False Positive: 383
 False Negative: 1964
4. Random Forest 
 False Positive: 267
 False Negative: 1877
 
- When a transaction is predicted as fraud but it is actually valid, we have a false positive. On the other hand, when a transaction is predicted as valid and it is actually a fraud, we have a false negative.
- We expect both  False Positive and False Negative are low so we get the high accuracy but we not expect the zero for False Positive in case of Logictic regression. In this case, we allow 4176 cases of fraud transaction and this will cost us a lot.
- In case of three other model, the number of fraud transaction that allow into the system is smaller around 1800-1900 cases. Those cases are our lost. In other hand, we also wrongly predict 260-380 customers as fraud transaction but they are really not. With False Positive, we actually don't get money from customer but not lose any money to them. 
- Considered about the cost, the lower False negative is the better model. And in this case is the Random Forest model. We can also use Decision Tree.

##### 4. ROC curve
- The ROC curve recommends that Random Forest and Decision Tree are best models. 
